<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#environment">Setting up environment</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Orea will to facilitate the share of patient condition between caregivers within intensive care units.

### Built With

Special thanks to all these open-source modules that were helpful to build Orea.
* [React](https://reactjs.org/)
* [Material-ui v5](https://mui.com/material-ui/getting-started/overview/)
* [react-redux](https://react-redux.js.org/)
* [typescript](https://www.typescriptlang.org/)
* [react-jsonschema-form](https://react-jsonschema-form.readthedocs.io/en/latest/)
* [react-markdown](https://github.com/remarkjs/react-markdown)
* [lodash](https://lodash.com/)
  

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* [npm](https://github.com/nodesource/distributions/blob/master/README.md)
  ```sh
  curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
  sudo apt-get install -y nodejs
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://framagit.org/interhop/orea/front-end.git
   cd front-end
   ```
2. Run npm
   ```sh
   npm install
   npm audit fix
   ```
3. Create and update .env file giving .env.example
4. Then you can either run in live (and access on a browser at localhost:3000)
   ```sh
   npm run start
   ```
5. (optional) If you want to run locally, in order for the authentication cookies to be sent, you need to learn to install/configure nginx. Use .templates/dev.nginx.conf (should be placed in /etc/nginx/sites-enable) as an example.
6. Or build the website. Result will be accessible in build folder
   ```sh
   npm run build
   ```

As a good url for back-end, you can either use an official [demo version online](https://orea-demo.api.interhop.org).

Or you can install your version of the backend, following [the instructions here](https://framagit.org/interhop/orea/back-end/-/blob/develop/README.md)

### Environment

*REACT_APP_DEV_MODE*:
'true' will allow devTools for redux store in browser

*REACT_APP_BASE_URL*:
Http url towards backend server api

*REACT_APP_GITLAB_MAILTO*:
Email address for the user to automatically generate a suggestion/report email to a development team.

*REACT_APP_DELAY_TOKEN_REFRESH_BEFORE_EXPIRATION*:
Amount of seconds before access token expiration after what the client will add a token */refresh* request with the next request.

*REACT_APP_INACTIVITY_DELAY_BEFORE_EXPIRATION*:
Amount of seconds accepted after last user interaction (keypress, click) to automatically */refresh* the access token if it is about to expire

<!-- CONTRIBUTING -->
## Contributing

1. Clone the Project
2. Checkout to develop branch (`git checkout develop`)
3. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
4. Think about using `npm run lint` to make sure your code is great
5. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
6. (optional) In commit message, add a line `interhop/orea/issues#{issueIid}`
7. Push to the Branch (`git push -u origin feature/AmazingFeature`)
8. Open a Pull Request with develop branch


<!-- CONTACT -->
## Contact

Alexandre Martin, main developer - [@w848](https://framagit.org/w848) - alexandre.gsm.martin@tutanota.com
