declare global {
  interface Window {
    env: any
  }
}

type EnvType = {
  REACT_APP_DEV_MODE: string
  REACT_APP_BASE_URL: string
  REACT_APP_GITLAB_MAILTO: string
  REACT_APP_DELAY_TOKEN_REFRESH_BEFORE_EXPIRATION: number
  REACT_APP_INACTIVITY_DELAY_BEFORE_EXPIRATION: number
}

const env: EnvType = { ...process.env, ...window.env }
export default env
