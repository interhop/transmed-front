import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackListResponse, BasicFilter, User } from 'types'

export type UserFilters = BasicFilter<User> & {
  authorizedReanimationServices__unit__bed?: string
}

export interface IServicesUsers {
  fetchUsers: (params?: UserFilters) => Promise<AxiosResponse<BackListResponse<User>> | AxiosError<any>>
}

const servicesUsers: IServicesUsers = {
  fetchUsers: (params) => {
    console.log(`Fetching users: ${config.apiPath.users}`, params)
    return api.get(config.apiPath.users, { params })
  }
}

export default servicesUsers
