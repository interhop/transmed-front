import { AxiosInstance } from 'axios'
// import { BackListResponse } from 'types'

// interface RestServices<T> {
//   list(params: Partial<T>): Promise<AxiosResponse<BackListResponse<T>>>
//   get(id: any): Promise<AxiosResponse<T>>
//   patch(id: any, update: Partial<T>): Promise<AxiosResponse<T>>
//   create(instance: T): Promise<AxiosResponse<T>>
//   delete(id: any): Promise<AxiosResponse>
// }

export const RestServicesBuilder = (api: AxiosInstance, objectUrl: string) => ({
  list: (params: any) => {
    console.log(`Fetching list: ${objectUrl}`, params)
    return api.get(objectUrl, { params })
  },
  get: (id: any) => {
    console.log(`Retrieving: ${objectUrl}/${id}`)
    return api.get(`${objectUrl}/${id}`)
  },
  patch: (id: any, update: any) => {
    console.log(`Patching: ${objectUrl}/${id}`, update)
    return api.patch(`${objectUrl}/${id}`, update)
  },
  create: (instance: any) => {
    console.log(`Creating: ${objectUrl}`, instance)
    return api.post(`${objectUrl}`, instance)
  },
  delete: (id: any) => {
    console.log(`Deleting: ${objectUrl}/${id}`)
    return api.delete(`${objectUrl}/${id}`)
  }
})
