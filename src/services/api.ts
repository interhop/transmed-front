import axios, { AxiosError, AxiosResponse, isAxiosError } from 'axios'
import config from 'config'
import jwt, { JwtPayload } from 'jwt-decode'

import RoutesConfig from 'components/Routes/AppNavigation/config'
import env from 'env'
import { clearLocalStorage } from 'state/storage'
import { BackValidationError, RefreshResult } from 'types'

const { axiosConfig, apiPath } = config

const api = axios.create(axiosConfig)

export const getCsrfToken = (): string =>
  document.cookie
    ?.split(';')
    .find((s) => s.includes('=') && s.split('=')[0] === 'csrftoken')
    ?.split('=')[1] ?? ''

api.interceptors.request.use((config) => {
  if (!!config.headers) {
    config.headers['X-CSRFToken'] = getCsrfToken()
  }
  return config
})

/**
 * If localStorage has accessToken stored, will decode it and process extra /refresh request
 * if half its longevity has passed since its time of issuance
 */
api.interceptors.request.use((config) => {
  if (
    config.url?.startsWith(apiPath.logout) ||
    config.url?.startsWith(apiPath.refresh) ||
    config.url?.startsWith(apiPath.login)
  )
    return config

  const accessToken = localStorage.getItem('accessToken')
  let jwtAt: any | JwtPayload
  try {
    jwtAt = accessToken && jwt<JwtPayload>(accessToken)
  } catch (e) {
    return config
  }
  if (!jwtAt || !jwtAt.exp) return config

  if (new Date().valueOf() > (jwtAt.exp - env.REACT_APP_DELAY_TOKEN_REFRESH_BEFORE_EXPIRATION) * 1000) {
    if (localStorage.getItem('refreshPending')) return config
    localStorage.setItem('refreshPending', '1')
    api
      .post(apiPath.refresh)
      .then((resp: AxiosResponse<RefreshResult> | AxiosError<BackValidationError>) => {
        if (isAxiosError(resp)) return
        resp.data.access && localStorage.setItem('accessToken', resp.data.access)
      })
      .finally(() => localStorage.removeItem('refreshPending'))
  }
  return config
})

/**
 * If one request returns 401, it is a hard way to realise that the user is not authentified anymore by the server
 * Hence, will redirect the user to login page
 */
api.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    if (
      error.response.status === 401 &&
      !window.location.href.startsWith(apiPath.login) &&
      !window.location.href.startsWith(apiPath.logout)
    ) {
      clearLocalStorage()
      api.post(apiPath.logout)
      window.location.replace(RoutesConfig.login.getPath(window.location.pathname + window.location.search))
    }
    return error
  }
)

export default api
