import { AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackListResponse, Bed } from 'types'

export interface IServicesBeds {
  fetchBed: (bedId: string) => Promise<AxiosResponse<Bed>>
  fetchBeds: (params?: Partial<Bed>) => Promise<AxiosResponse<BackListResponse<Bed>>>
}

const servicesBeds: IServicesBeds = {
  fetchBed: (bedId: string) => {
    const url = `${config.apiPath.beds}/${bedId}`
    console.log(`Fetching bed at: ${url}`)
    return api.get(url)
  },
  fetchBeds: (params) => {
    console.log(`Fetching beds: ${config.apiPath.beds}`, params)
    return api.get(config.apiPath.beds, { params })
  }
}

export default servicesBeds
