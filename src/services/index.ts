import servicesAccesses, { IServicesAccesses } from './accesses'
import servicesBeds, { IServicesBeds } from './beds'
import servicesBackend, { IServicesBackend } from './backend'
import servicesIntensiveCareServices, { IServicesIntensiveCareServices } from './intensiveCareServices'
import servicesIssues, { IServicesIssues } from './issues'
import servicesStays, { IServicesStays } from './stays'
import servicesPatients, { IServicesPatients } from './patients'
import servicesUnits, { IServicesUnits } from './units'
import servicesUser, { IServicesUser } from './user'
import servicesUsers, { IServicesUsers } from './users'

export interface IServices {
  accesses: IServicesAccesses
  backend: IServicesBackend
  beds: IServicesBeds
  intensiveCareServices: IServicesIntensiveCareServices
  issues: IServicesIssues
  patients: IServicesPatients
  stays: IServicesStays
  units: IServicesUnits
  user: IServicesUser
  users: IServicesUsers
}

const services: IServices = {
  accesses: servicesAccesses,
  backend: servicesBackend,
  beds: servicesBeds,
  intensiveCareServices: servicesIntensiveCareServices,
  issues: servicesIssues,
  patients: servicesPatients,
  stays: servicesStays,
  units: servicesUnits,
  user: servicesUser,
  users: servicesUsers
}

export default services
