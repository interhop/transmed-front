import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackValidationError, LoginResult, RefreshResult, User } from 'types'

export interface IServicesUser {
  /**
   * Fonction qui permet d'authentifier un utilisateur avec un username et un password
   *
   * Argument:
   *  - username: Identifiant du practitioner
   *  - password: Mot de passe du practitioner
   *
   * Retourne la reponse de Axios
   */
  login: (username: string, password: string) => Promise<AxiosResponse<LoginResult> | AxiosError<BackValidationError>>

  /**
   * Fonction qui permet de rafraîchir les tokens access et refresh
   *
   * Retourne la reponse de Axios
   */
  refresh: () => Promise<AxiosResponse<RefreshResult> | AxiosError<BackValidationError>>

  /**
   * Cette fonction permet d'appeler la route de logout
   *
   */
  logout: () => Promise<void>

  /**
   * Cette fonction permet de modifier personal_notes
   *
   */
  submitPersonalNotes: (user: User, notes: string) => Promise<any> // AxiosResponse<BackEndUser>>
  /**
   * Simple get request to login url to receive a csrf cookie
   */
  getCsrfToken: () => Promise<any>
}

const servicesUser: IServicesUser = {
  login: (username, password) => {
    // try {
    const formData = new FormData()
    formData.append('username', username.toString())
    formData.append('password', password)

    return api.post(config.apiPath.login, formData)
    // } catch (error) {
    //   console.error("erreur lors de l'éxécution de la fonction authenticate", error)
    //   return error
    // }
  },
  refresh: () => {
    return api.post(config.apiPath.refresh)
  },
  logout: async () => {
    await api.post(config.apiPath.logout)
  },
  submitPersonalNotes: async (user, notes) => {
    return (await api.patch(`${config.apiPath.users}/${user.uuid}`, {
      personal_notes: notes
    })) as AxiosResponse<User>
  },
  getCsrfToken: async () => api.get(config.apiPath.login)
}

export default servicesUser
