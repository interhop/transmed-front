import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import {
  BackListResponse,
  BackValidationError,
  BasicFilter,
  PatientDetection,
  PatientFailure,
  PatientNote,
  Stay,
  TreatmentLimitation
} from 'types'

export interface IServicesStays {
  fetchStay: (stayId: string) => Promise<AxiosResponse<Stay>>
  fetchStays: (params?: BasicFilter<Stay>) => Promise<AxiosResponse<BackListResponse<Stay>>>
  createStay: (stay: Partial<Stay>) => Promise<AxiosResponse<Stay> | AxiosError<BackValidationError>>
  updateStay: (id: string, data: Partial<Stay>) => Promise<AxiosResponse<Stay>>
  closeStay: (id: string) => Promise<AxiosResponse<Stay> | AxiosError<BackValidationError>>
  moveStay: (id: string, newBedId: string) => Promise<AxiosResponse<Stay> | AxiosError<BackValidationError>>
  createDetection: (
    detection: Partial<PatientDetection>
  ) => Promise<AxiosResponse<PatientDetection> | AxiosError<BackValidationError>>
  createFailureMeasure: (
    failure: Partial<PatientFailure>
  ) => Promise<AxiosResponse<PatientFailure> | AxiosError<BackValidationError>>
  updateFailureMeasure: (
    failureId: string,
    failure: Partial<PatientFailure>
  ) => Promise<AxiosResponse<PatientFailure> | AxiosError<BackValidationError>>
  createTreatmentLimitation: (
    lat: Partial<TreatmentLimitation>
  ) => Promise<AxiosResponse<TreatmentLimitation> | AxiosError<BackValidationError>>
  updateTreatmentLimitation: (
    latId: string,
    lat: Partial<TreatmentLimitation>
  ) => Promise<AxiosResponse<TreatmentLimitation> | AxiosError<BackValidationError>>
  createNote: (note: Partial<PatientNote>) => Promise<AxiosResponse<PatientNote> | AxiosError<BackValidationError>>
  updateNote: (
    noteId: string,
    note: Partial<PatientNote>
  ) => Promise<AxiosResponse<PatientNote> | AxiosError<BackValidationError>>
}

const serviceStays: IServicesStays = {
  fetchStay: (stayId) => {
    const url = `${config.apiPath.patientStays}/${stayId}`
    console.log(`Fetching stay at: ${url}`)
    return api.get(url)
  },
  fetchStays: (params?) => {
    const url = `${config.apiPath.patientStays}`
    console.log(`Fetching stays at: ${url}`, params)
    return api.get(url, { params })
  },
  createStay: (stay) => {
    const url = `${config.apiPath.patientStays}`
    console.log(`Creating stay at: ${url}`, stay)
    return api.post(url, stay)
  },
  updateStay: (id, data) => {
    const url = `${config.apiPath.patientStays}/${id}`
    console.log(`Updating stay: ${url}`, data)
    return api.patch(url, data)
  },
  closeStay: (id) => {
    const url = `${config.apiPath.patientStays}/${id}/close`
    console.log(`Closing stay: ${url}`)
    return api.patch(url)
  },
  moveStay: (id, newBedId) => {
    const url = `${config.apiPath.patientStays}/${id}/move`
    console.log(`Moving stay to ${newBedId} at: ${url}`)
    return api.patch(url, { newBed: newBedId })
  },
  createDetection: (detection) => {
    const url = `${config.apiPath.detectionMeasures}`
    console.log(`Posting detection to: ${url}`, detection)
    return api.post(url, detection)
  },
  createFailureMeasure: (failure) => {
    const url = `${config.apiPath.failureMeasures}`
    console.log(`Posting failure to: ${url}`, failure)
    return api.post(url, failure)
  },
  updateFailureMeasure: (failureId, failure) => {
    const url = `${config.apiPath.failureMeasures}/${failureId}`
    console.log(`Updating failure at: ${url}`, failure)
    return api.patch(url, failure)
  },
  createTreatmentLimitation: (lat) => {
    // const url = `${config.apiPath.patientStays}/${lat.stay}/treatment-limitations`
    const url = `${config.apiPath.treatmentLimitations}`
    console.log(`Posting treat. limit. to: ${url}`, lat)
    return api.post(url, lat)
  },
  updateTreatmentLimitation: (id, lat) => {
    const url = `${config.apiPath.treatmentLimitations}/${id}`
    console.log(`Updating treat. limit. at: ${url}`, lat)
    return api.patch(url, lat)
  },
  createNote: (note) => {
    const url = `${config.apiPath.patientNotes}`
    console.log(`Creating note at: ${url}`, note)
    return api.post(url, note)
  },
  updateNote: (noteId, note) => {
    const url = `${config.apiPath.patientNotes}/${noteId}`
    console.log(`Updating note at: ${url}`, note)
    return api.patch(url, note)
  }
}

export default serviceStays
