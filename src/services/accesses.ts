import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackListResponse, BasicFilter, Access, FrontInput, BackValidationError } from 'types'

export type AccessFilters = BasicFilter<Access> & {
  authorizedReanimationServices__unit__bed?: string
}

export interface IServicesAccesses {
  fetchAccesses: (
    params?: AccessFilters
  ) => Promise<AxiosResponse<BackListResponse<Access>> | AxiosError<BackValidationError>>
  createAccess: (access: FrontInput<Access>) => Promise<AxiosResponse<Access> | AxiosError<BackValidationError>>
  updateAccess: (
    accessId: string,
    access: Partial<Access>
  ) => Promise<AxiosResponse<Access> | AxiosError<BackValidationError>>
  denyAccess: (accessId: string) => Promise<AxiosResponse<Access> | AxiosError<BackValidationError>>
  validateAccess: (accessId: string) => Promise<AxiosResponse<Access> | AxiosError<BackValidationError>>
  closeAccess: (accessId: string) => Promise<AxiosResponse<Access> | AxiosError<BackValidationError>>
  deleteAccess: (accessId: string) => Promise<AxiosResponse<null> | AxiosError<BackValidationError>>
}

const servicesAccesses: IServicesAccesses = {
  fetchAccesses: (params) => {
    console.log(`Fetching accesses: ${config.apiPath.accesses}`, params)
    return api.get(config.apiPath.accesses, { params })
  },
  createAccess: (access) => {
    console.log(`Posting access: ${config.apiPath.accesses}`, access)
    return api.post(config.apiPath.accesses, access)
  },
  updateAccess: (accessId, access) => {
    const url = `${config.apiPath.accesses}/${accessId}`
    console.log(`Updating access at: ${url}`, access)
    return api.patch(url, access)
  },
  denyAccess: (accessId) => {
    const url = `${config.apiPath.accesses}/${accessId}/deny`
    console.log(`Denying access at: ${url}`)
    return api.patch(url)
  },
  validateAccess: (accessId) => {
    const url = `${config.apiPath.accesses}/${accessId}/validate`
    console.log(`Validating access at: ${url}`)
    return api.patch(url)
  },
  closeAccess: (accessId) => {
    const url = `${config.apiPath.accesses}/${accessId}/close`
    console.log(`Closing access at: ${url}`)
    return api.patch(url)
  },
  deleteAccess: (accessId) => {
    const url = `${config.apiPath.accesses}/${accessId}`
    console.log(`Deleting access at: ${url}`)
    return api.delete(url)
  }
}

export default servicesAccesses
