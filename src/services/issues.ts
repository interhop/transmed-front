import { AxiosError, AxiosResponse } from 'axios'

import api from './api'
import config from 'config'
import { BackListResponse, BasicFilter, Issue, BackValidationError, PostedIssue } from 'types'

export type IssueFilters = Omit<BasicFilter<Issue>, 'labels'> & {
  labels?: string
  in?: 'title' | 'description' | 'title,description'
}

export interface IServicesIssues {
  fetchIssues: (
    params?: IssueFilters
  ) => Promise<AxiosResponse<BackListResponse<Issue>> | AxiosError<BackValidationError>>
  createIssue: (issue: Partial<PostedIssue>) => Promise<AxiosResponse<Issue> | AxiosError<BackValidationError>>
  voteIssue: (projectId: number, issueId: number) => Promise<AxiosResponse<Issue> | AxiosError<BackValidationError>>
  unvoteIssue: (projectId: number, issueId: number) => Promise<AxiosResponse<Issue> | AxiosError<BackValidationError>>
}

const servicesIssues: IServicesIssues = {
  fetchIssues: (params) => {
    console.log(`Fetching issues: ${config.apiPath.issues}`, params)
    return api.get(config.apiPath.issues, { params })
  },
  createIssue: (issue) => {
    const issueForm = new FormData()

    issueForm.append('title', issue.title || '')
    if (issue.label) issueForm.append('label', issue.label || '')
    issueForm.append('isIncident', issue.isIncident?.toString() ?? 'false')
    issueForm.append('description', issue.description || '')
    if (issue.attachment && issue.attachment.length > 0) {
      issueForm.append('attachment', issue.attachment[0])
    }
    console.log(`Posting issue: ${config.apiPath.issues}`, issueForm)

    return api.post(config.apiPath.issues, issueForm, {
      headers: {
        'content-type': 'multipart/form-data'
      }
    })
  },
  voteIssue: (projectId: number, issueId: number) => {
    const url = `${config.apiPath.issues}/${projectId}-${issueId}/vote`
    console.log(`Voting for issue at: ${url}`)
    return api.patch(url)
  },
  unvoteIssue: (projectId: number, issueId: number) => {
    const url = `${config.apiPath.issues}/${projectId}-${issueId}/unvote`
    console.log(`Unvoting for issue at: ${url}`)
    return api.patch(url)
  }
}

export default servicesIssues
