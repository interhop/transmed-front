import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'Faq'
export const classes = {
  card: `${PREFIX}-card`,
  section: `${PREFIX}-section`,
  sectionTitle: `${PREFIX}-sectionTitle`,
  questionItem: `${PREFIX}-questionItem`,
  question: `${PREFIX}-question`,
  answer: `${PREFIX}-answer`,
  input: `${PREFIX}-input`,
  emailLink: `${PREFIX}-emailLink`,
  sendButton: `${PREFIX}-sendButton`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  margin: theme.spacing(2, 0, 2, 0),
  [`& .${classes.card}`]: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2, 0, 2, 0),
    // backgroundColor: theme.palette.primary.light,
    border: `2px solid ${theme.palette.primary.light}`,
    borderRadius: 12,
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)'
  },
  [`& ::before`]: {
    backgroundColor: 'unset'
  },
  [`& .${classes.section}`]: {
    margin: theme.spacing(2)
  },
  [`& .${classes.sectionTitle}`]: {
    margin: theme.spacing(2),
    textAlign: 'center'
  },
  [`& .${classes.questionItem}`]: {
    backgroundColor: alpha(theme.palette.primary.light, 0.2),
    margin: theme.spacing(4, 0, 4, 0),
    borderRadius: 6,
    boxShadow: '0px 4px 3px rgba(0,0,0,0.15)'
  },
  [`& .${classes.question}`]: {
    // backgroundColor: alpha(theme.palette.background.paper, 0.2),
    borderTopRightRadius: 6,
    borderTopLeftRadius: 6,
    paddingLeft: theme.spacing(0, 2, 0, 2),
    color: theme.palette.text.primary,
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  [`& .${classes.answer}`]: {
    borderTopRightRadius: 6,
    borderTopLeftRadius: 6,
    color: theme.palette.text.primary
  },
  [`& .${classes.input}`]: {},
  [`& .${classes.emailLink}`]: {
    cursor: 'pointer',
    color: theme.palette.secondary.main
  },
  [`& .${classes.sendButton}`]: {
    width: theme.spacing(60),
    margin: theme.spacing(4),
    backgroundColor: theme.palette.secondary.main,
    borderRadius: 24,
    color: theme.palette.secondary.contrastText,
    '&:hover': {
      backgroundColor: theme.palette.secondary.light
    },
    '&.MuiLoadingButton-loading': {
      color: 'transparent'
    },
    '&.Mui-disabled': {
      backgroundColor: theme.palette.action.disabledBackground
    }
  }
}))
