import React from 'react'
import _ from 'lodash'
import { View, Text } from '@react-pdf/renderer'

import { PrintSettings, PrintSettingsComponent } from 'state/settings'

import PdfDemographicData from './Components/DemographicData'
import PdfDetections, { detectionsEstimatedNbLines } from './Components/Detections'
import PdfFailures from './Components/Failures'
import PdfInfoCell from './Components/InfoCell'
import PdfListLabels, { listLabelsEstimatedNbLines } from './Components/ListLabels'
import PdfPatientNote, { patientNoteEstimatedNbLines } from './Components/PatientNote'
import PdfTreatmentLimitations, { treatmentLimitationsEstimatedNbLines } from './Components/TreatmentLimitations'

import { pdfStyles } from './styles'
import { Patient, SeverityType, Stay, User } from 'types'

const severityValues: { [Key in SeverityType]: string } = { 0: 'Haute', 1: 'Moyenne', 2: 'Faible' }

type SizedComponent = {
  component: JSX.Element
  estimatedNbLines: number
}

type PdfComponentsProps = {
  sizedComponents: SizedComponent[]
  maxNbCols?: number
  flexGrow?: number
}

const PdfComponents: React.FC<PdfComponentsProps> = ({ sizedComponents, maxNbCols, flexGrow }) => {
  const styles = pdfStyles()
  const componentGroups: JSX.Element[][] = []
  let currNbLines: number

  if (maxNbCols) {
    const nbLinesPerCol =
      (1 + sizedComponents.length + sizedComponents.map((c) => c.estimatedNbLines).reduce((a, b) => a + b, 2)) /
      maxNbCols

    sizedComponents.forEach((c) => {
      if (
        componentGroups.length < maxNbCols &&
        (!currNbLines || 1 + currNbLines + c.estimatedNbLines / 2 > nbLinesPerCol)
      ) {
        componentGroups.push([c.component])
        currNbLines = c.estimatedNbLines + 2
      } else {
        componentGroups[componentGroups.length - 1].push(c.component)
        currNbLines = 1 + currNbLines + c.estimatedNbLines
      }
    })
  } else {
    componentGroups.push(_.map(sizedComponents, 'component'))
  }

  return (
    <>
      {componentGroups.map((Components, i) =>
        _.filter(Components).length ? (
          <View
            key={i}
            wrap={!!flexGrow}
            style={[{ flexGrow, flexDirection: flexGrow ? 'column' : 'row' }, styles.components]}
          >
            {Components.map(
              (Component, j) =>
                Component && (
                  <View key={`${i}-${j}`} style={[{ flexGrow }, styles.component]}>
                    {Component}
                  </View>
                )
            )}
          </View>
        ) : (
          <></>
        )
      )}
    </>
  )
}

type PdfStayProps = {
  data: Stay
  user: User | null
  settings?: PrintSettings
  withSituation?: boolean
  fontSize: number
}

const PdfStay: React.FC<PdfStayProps> = ({ data, user, settings, fontSize, withSituation = true }) => {
  // SEVERITY
  const severityValue = severityValues[data.severity]
  const generalNotes = data.notes?.filter(_.matches({ type: 'general' }))
  const generalNote =
    generalNotes?.find(_.matches({ createdBy: user?.uuid })) ??
    _.maxBy(generalNotes, (n) => new Date(n.updatedAt || 0).valueOf())
  const lat = _.maxBy(data.treatmentLimitations, (lat) => new Date(lat.measureDatetime).valueOf())
  const evolution = data.notes?.find(_.matches({ type: 'evolution' }))
  const todoNote = data.notes?.find(_.matches({ type: 'todo' }))
  const rdhNote = data.notes?.find(_.matches({ type: 'rdh' }))

  const isRequired = (component: PrintSettingsComponent) =>
    !settings?.components || _.includes(settings.components, component)
  const patient = data.patient as Patient

  const maxWidths = {
    large: 0,
    medium: settings?.onePatientPerPage ? 285 : 140,
    small: settings?.onePatientPerPage ? 190 : 90
    // large: isRequired('measuresTable') ? (settings.nbDays > 5 ? 295 : 195) : 0,
    // medium: isRequired('measuresTable') ? (settings.nbDays > 5 ? 136 : 140) : 140,
    // small: isRequired('measuresTable') ? (settings.nbDays > 5 ? 90 : 90) : 90
  }

  const smallComps: { [Key in string]: SizedComponent | undefined } = {
    SeverityField: {
      component: <PdfInfoCell label="Gravité" value={severityValue} fontSize={fontSize} />,
      estimatedNbLines: 1
    },
    Depistages: !isRequired('detections')
      ? undefined
      : {
          component: <PdfDetections detections={data.detections} fontSize={fontSize} maxWidth={maxWidths.small} />,
          estimatedNbLines: detectionsEstimatedNbLines(data.detections)
        },
    Antecedents: !isRequired('antecedents')
      ? undefined
      : {
          component: (
            <PdfListLabels
              title="Antécédents"
              labels={patient.antecedents}
              fontSize={fontSize}
              maxWidth={maxWidths.small}
            />
          ),
          estimatedNbLines: listLabelsEstimatedNbLines(patient.antecedents, maxWidths.small, fontSize)
        },
    Allergies: !isRequired('allergies')
      ? undefined
      : {
          component: (
            <PdfListLabels
              title="Allergies"
              labels={patient.allergies}
              fontSize={fontSize}
              maxWidth={maxWidths.small}
            />
          ),
          estimatedNbLines: listLabelsEstimatedNbLines(patient.allergies, maxWidths.small, fontSize)
        },
    LatText: !isRequired('lat')
      ? undefined
      : {
          component: <PdfTreatmentLimitations lat={lat} fontSize={fontSize} maxWidth={maxWidths.small} />,
          estimatedNbLines: treatmentLimitationsEstimatedNbLines(lat, maxWidths.small, fontSize)
        },
    Failures: !isRequired('failures')
      ? undefined
      : {
          component: <PdfFailures failures={data.failureMeasures} fontSize={fontSize} maxWidth={maxWidths.small} />,
          estimatedNbLines: 3
        }
  }

  const mediumComps: { [Key in string]: SizedComponent | undefined } = {
    PatientInfos: {
      component: (
        <PdfDemographicData stay={data} fontSize={fontSize} withSituation={withSituation} maxWidth={maxWidths.medium} />
      ),
      estimatedNbLines: 8
    },
    GeneralNote: !isRequired('dailyNotes')
      ? undefined
      : {
          component: (
            <PdfPatientNote title="Note générale" note={generalNote} fontSize={fontSize} maxWidth={maxWidths.medium} />
          ),
          estimatedNbLines: patientNoteEstimatedNbLines(generalNote, maxWidths.medium, fontSize)
        },
    RecentDiseaseHistory: !isRequired('recentDiseaseHistory')
      ? undefined
      : {
          component: (
            <PdfPatientNote
              title="Histoire de la maladie récente"
              note={rdhNote}
              fontSize={fontSize}
              maxWidth={maxWidths.medium}
            />
          ),
          estimatedNbLines: patientNoteEstimatedNbLines(rdhNote, maxWidths.medium, fontSize)
        },
    Evolution: !isRequired('evolution')
      ? undefined
      : {
          component: (
            <PdfPatientNote title="Evolution" note={evolution} fontSize={fontSize} maxWidth={maxWidths.medium} />
          ),
          estimatedNbLines: patientNoteEstimatedNbLines(evolution, maxWidths.medium, fontSize)
        },
    TodoList: !isRequired('todoList')
      ? undefined
      : {
          component: (
            <PdfPatientNote
              title="Todo list"
              note={todoNote}
              isChecklist
              fontSize={fontSize}
              maxWidth={maxWidths.medium}
            />
          ),
          estimatedNbLines: patientNoteEstimatedNbLines(evolution, maxWidths.medium, fontSize, true)
        },
    BlankView: !isRequired('writingSpace')
      ? undefined
      : {
          component: (
            <View style={{ width: maxWidths.medium, height: maxWidths.medium * 0.4 }}>
              <Text style={{ fontSize }}>Prise de notes</Text>
            </View>
          ),
          estimatedNbLines: 5
        }
  }

  return (
    <View wrap={false}>
      <View
        style={{
          flexDirection: 'row'
        }}
      >
        <View
          style={{
            flexDirection: 'column'
          }}
        >
          <PdfComponents sizedComponents={_.filter(_.values(mediumComps)).slice(0, 2) as SizedComponent[]} />
          <View
            style={{
              flexDirection: 'row'
            }}
          >
            <PdfComponents
              maxNbCols={settings?.onePatientPerPage ? 3 : 2}
              sizedComponents={_.filter(_.values(smallComps)) as SizedComponent[]}
              flexGrow={3}
            />
          </View>

          <PdfComponents sizedComponents={_.filter(_.values(mediumComps)).slice(2) as SizedComponent[]} />
        </View>
      </View>
    </View>
  )
}

export default PdfStay
