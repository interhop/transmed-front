import React from 'react'

import { Text, View } from '@react-pdf/renderer'

import { pdfStyles } from './pdfStyles'

type PdfInfoCellProps = {
  value?: string | number
  infoToAdd?: string
  label?: string
  fontSize: number
}

const PdfInfoCell: React.FC<PdfInfoCellProps> = ({ value, infoToAdd, label, fontSize }) => {
  const styles = pdfStyles(fontSize)
  return (
    <View style={styles.pdfFlexRow}>
      <Text style={[styles.bold, styles.pdfText]}>{label}</Text>
      <Text style={styles.pdfText}>{value}</Text>
      {infoToAdd && <Text style={[styles.italic, styles.additionalInfo, styles.pdfText]}>{infoToAdd}</Text>}
    </View>
  )
}

export default PdfInfoCell
