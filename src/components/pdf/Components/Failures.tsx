import React from 'react'
import _ from 'lodash'

import ReactPDF, { Text, View } from '@react-pdf/renderer'
import ImageProps = ReactPDF.ImageProps

import { pdfStyles } from './pdfStyles'
import { HeartFailureIconPdf } from 'assets/icons/heart_failure'
import { BioChemicalFailureIconPdf } from 'assets/icons/bio_chemical_failure'
import { LungFailureIconPdf } from 'assets/icons/lung_failure'
import { KidneyFailureIconPdf } from 'assets/icons/kidney_failure'
import { LiverFailureIconPdf } from 'assets/icons/liver_failure2'
import { HematologicFailureIconPdf } from 'assets/icons/hematologic_failure'
import { BrainFailureIconPdf } from 'assets/icons/brain_failure'
import { dateTimeToStr } from 'assets/utils'
import { FailureType, PatientFailure } from 'types'

const failuresIcons: { [Key in FailureType]: React.FC<Partial<ImageProps>> } = {
  heart: HeartFailureIconPdf,
  metabolic: BioChemicalFailureIconPdf,
  brain: BrainFailureIconPdf,
  lung: LungFailureIconPdf,
  kidney: KidneyFailureIconPdf,
  liver: LiverFailureIconPdf,
  hematologic: HematologicFailureIconPdf,
  detection: () => <></>
}

type PdfFailuresProps = {
  failures?: PatientFailure[]
  fontSize: number
  maxWidth: number
}

// todo check how to use flexGrow
const PdfFailures: React.FC<PdfFailuresProps> = ({ failures = [], fontSize, maxWidth }) => {
  const styles = pdfStyles(fontSize)

  return (
    <View id="failures" style={{ maxWidth }}>
      <Text style={[styles.pdfText, styles.bold]}>Défaillances</Text>
      <View
        style={{
          flexDirection: 'column',
          flexGrow: 1,
          justifyContent: 'flex-start'
        }}
      >
        {failures.filter(_.matches({ active: true })).map((f) => (
          <View key={`view-${f.failureType}`} style={{ flexGrow: 1, alignItems: 'flex-start', flexDirection: 'row' }}>
            {failuresIcons[f.failureType]({ style: [styles.icon] })}
            <Text style={[styles.pdfText, { flexGrow: 1 }]} wrap>
              {f.indication} ({dateTimeToStr(f.measureDatetime)})
            </Text>
          </View>
        ))}
      </View>
    </View>
  )
}

export default PdfFailures
