import React from 'react'
import _ from 'lodash'

import { Text, View } from '@react-pdf/renderer'

import { pdfStyles } from './pdfStyles'
import { estimateNbLines } from 'assets/utils'

export const listLabelsEstimatedNbLines = (listItems: { [p: string]: string }, maxWidth: number, fontSize: number) => {
  const lineLength = (k: string, v: string) => 4 + k.length + v.length

  return (
    _.entries(listItems)
      .map(([k, v]) => estimateNbLines(lineLength(k, v), maxWidth, fontSize))
      .reduce((a, b) => a + b, 0) + 1
  ) //+ 1 for title
}

type PdfListLabelsProps = {
  title: string
  labels: { [Key: string]: string }
  fontSize: number
  maxWidth: number
}

const PdfListLabels: React.FC<PdfListLabelsProps> = ({ title, labels, fontSize, maxWidth }) => {
  const styles = pdfStyles(fontSize)

  return (
    <View style={{ maxWidth }}>
      <Text style={[styles.pdfText, styles.bold]}>
        {title}
        {_.keys(labels).length === 0 && ': aucun'}
      </Text>
      <Text style={styles.pdfText}>
        {_.keys(labels).length > 0 &&
          '- ' +
            _.entries(labels)
              .map(([k, v]) => `${k}: ${v}`)
              .join(`\n- `)}
      </Text>
    </View>
  )
}

export default PdfListLabels
