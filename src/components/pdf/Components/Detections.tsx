import React from 'react'
import _ from 'lodash'

import { Text, View } from '@react-pdf/renderer'

import { pdfStyles } from './pdfStyles'
import { detectionsToInfos } from 'components/BedStay/Detections/Detections'
import { PatientDetection } from 'types'

type PdfDateInfoProps = {
  label: string
  date: string
  styles: any
}

const PdfDateInfo: React.FC<PdfDateInfoProps> = ({ label, date, styles }) => {
  return (
    <View style={styles.pdfFlexRow}>
      <Text style={[styles.pdfText, styles.bold]}>{label}: </Text>
      <Text style={[styles.pdfText]}>{date}</Text>
    </View>
  )
}

export const detectionsEstimatedNbLines = (detections: PatientDetection[] = []) => {
  const infos: { date?: string; label: string }[] = detectionsToInfos(detections)
  return _.filter(_.map(infos, 'date')).length + 1
}

type PdfDetectionsProps = {
  detections?: PatientDetection[]
  fontSize: number
  maxWidth: number
}

const PdfDetections: React.FC<PdfDetectionsProps> = ({ detections = [], fontSize, maxWidth }) => {
  const styles = pdfStyles(fontSize)

  const infos: { date?: string; label: string }[] = detectionsToInfos(detections)

  return (
    <View style={{ maxWidth }}>
      <Text style={[styles.pdfText, styles.bold]}>Dépistages ER et ORL</Text>
      {_.filter(infos, 'date').map((d, i) => (
        <PdfDateInfo key={`pdfdateinfo-${i}`} label={d.label} date={d.date ?? ''} styles={styles} />
      ))}
    </View>
  )
}

export default PdfDetections
