import React from 'react'
import _ from 'lodash'

import { JSONSchema7 } from 'json-schema'
import { Text, View } from '@react-pdf/renderer'

import { pdfStyles } from './pdfStyles'
import { treatmentLimitationFormSchema } from 'components/formSchemas'
import { dateTimeToStr, estimateNbLines } from 'assets/utils'
import { TreatmentLimitation } from 'types'

export const treatmentLimitationsEstimatedNbLines = (
  lat: TreatmentLimitation | undefined,
  maxWidth: number,
  fontSize: number
) => {
  return _.sum(
    _.entries(lat).map(([k, v]) =>
      v
        ? typeof v === 'boolean'
          ? estimateNbLines(6 + k.length, maxWidth, fontSize)
          : estimateNbLines(4 + k.length + v.toString().length, maxWidth, fontSize)
        : 0
    )
  ) // + 2 for title and 'last edited' line
}

type PdfTreatmentLimitationsProps = {
  lat?: TreatmentLimitation
  fontSize: number
  maxWidth: number
}

const PdfTreatmentLimitations: React.FC<PdfTreatmentLimitationsProps> = ({ lat, fontSize, maxWidth }) => {
  const styles = pdfStyles(fontSize)
  return (
    <View style={{ maxWidth }}>
      <Text style={[styles.pdfText, styles.bold]}>LAT</Text>
      {lat && <Text style={[styles.pdfText, styles.italic]}>Modifié le: {dateTimeToStr(lat.measureDatetime)}</Text>}
      <Text style={styles.pdfText}>
        {lat
          ? (_.values(treatmentLimitationFormSchema.properties).filter((p) => typeof p !== 'boolean') as JSONSchema7[])
              .map(({ properties }) =>
                _.entries(properties)
                  .filter(([k]) => !!_.propertyOf(lat)(k))
                  .map(([k, prop]) =>
                    typeof prop !== 'boolean'
                      ? prop.type === 'boolean'
                        ? `- ${prop.title}`
                        : `- ${prop.title}: ${_.propertyOf(lat)(k)}`
                      : ''
                  )
              )
              .reduce((a, b) => _.concat(a, b))
              .filter(_.identity)
              .join(`\n`)
          : 'Vide'}
      </Text>
    </View>
  )
}

export default PdfTreatmentLimitations
