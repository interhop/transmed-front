// export {}
import React from 'react'

import { Text, View } from '@react-pdf/renderer'

import PdfInfoCell from './InfoCell'
import { pdfStyles } from './pdfStyles'
import { dateToDayStep, dateToStr, getAge } from 'assets/utils'
import { Patient, Stay } from 'types'

const buildIMC = (weight: number | null, size: number | null): string => {
  const imc = weight && size ? weight / (size / 100) ** 2 : NaN
  return `(IMC: ${isNaN(imc) ? '?' : imc.toFixed(2)})`
}

type PdfDemographicDataProps = {
  stay: Stay
  fontSize: number
  withSituation: boolean
  maxWidth: number
}

const PdfDemographicData: React.FC<PdfDemographicDataProps> = ({ stay, fontSize, withSituation, maxWidth }) => {
  const styles = pdfStyles(fontSize)

  const patient = stay.patient as Patient

  return (
    <View style={{ maxWidth }}>
      <View style={{ flexGrow: 4 }}>
        <Text style={[styles.pdfText, styles.subTitle]}>Séjour</Text>
        {withSituation && <PdfInfoCell value={stay.bedDescription} label="Situation" fontSize={fontSize} />}

        <PdfInfoCell label="Motif d'admission" value={stay.hospitalisationCause} fontSize={fontSize} />

        <PdfInfoCell
          label="Date d'entrée"
          value={dateToStr(stay.startDate)}
          infoToAdd={`(${dateToDayStep(stay.startDate)})`}
          fontSize={fontSize}
        />
      </View>

      <View style={{ flexGrow: 5 }}>
        <Text style={[styles.pdfText, styles.subTitle]}>Informations démographiques</Text>
        <View style={{ flexDirection: 'row' }}>
          <View>
            <PdfInfoCell label="NIP" value={patient.localId} fontSize={fontSize} />
            <PdfInfoCell label="Nom" value={patient.lastName} fontSize={fontSize} />
            <PdfInfoCell label="Prénom" value={patient.firstName} fontSize={fontSize} />
            <PdfInfoCell label="Sexe" value={patient.sex} fontSize={fontSize} />
          </View>

          <View>
            <PdfInfoCell
              label="Naissance"
              value={patient.birthDate}
              infoToAdd={patient.birthDate && `(${getAge(patient.birthDate)} ans)`}
              fontSize={fontSize}
            />
            <PdfInfoCell label="Poids (kg)" value={patient.weightKg || '?'} fontSize={fontSize} />
            <PdfInfoCell
              label="Taille (cm)"
              infoToAdd={buildIMC(patient.weightKg, patient.sizeCm)}
              value={patient.sizeCm || '?'}
              fontSize={fontSize}
            />
          </View>
        </View>
      </View>
    </View>
  )
}

export default PdfDemographicData
