import React from 'react'
import _ from 'lodash'

import { Checkbox, Chip, Unstable_Grid2 as Grid, Skeleton, SvgIconProps, Tooltip, Typography } from '@mui/material'

import { classes, Root } from './styles'
import { BedIcon, ToWatchIcon } from 'assets/icons'
import { useAppDispatch, useAppSelector } from 'state'
import { selectBeds, unselectBeds } from 'state/selection'
import { Unit, Bed as BedType, Bed } from 'types'

const DisplayedInfo: React.FC<{ title: string; value: any; icon: React.FC<SvgIconProps> }> = (props) => {
  return (
    <Root>
      <Tooltip title={props.title} arrow>
        <Chip className={classes.info} avatar={<props.icon className={classes.infoIcon} />} label={props.value} />
      </Tooltip>
    </Root>
  )
}

const UnitTitle: React.FC<{ unit: Unit }> = ({ unit }) => {
  const dispatch = useAppDispatch()
  const { display, selectedBeds, beds, stays } = useAppSelector((state) => ({
    display: state.settings,
    selectedBeds: state.selection.beds,
    beds: state.beds,
    stays: state.stays.entities
  }))

  const unitBeds = _.filter(_.map(unit.beds, _.propertyOf(beds.entities))) as Bed[]

  const computeNbSeverePatients = (beds: BedType[]) => {
    return beds.filter((b) => b.currentPatientStay && stays[b.currentPatientStay]?.severity === 0).length
  }

  const computeNbAvailableBeds = (beds: BedType[]) => {
    return beds.filter((b) => !b.currentPatientStay).length
  }

  const busyBeds: string[] =
    beds.status !== 'loading' ? unit.beds?.filter((id) => !!beds.entities[id]?.currentPatientStay) : []

  const isChecked = busyBeds.every((id) => _.includes(selectedBeds, id))

  return (
    <Root>
      <Grid container justifyContent="flex-start" alignItems="center" spacing={2} className={classes.subHeaderContent}>
        <Grid>
          <Typography align="center" variant="h6" className={`${classes.unitTitle}`}>
            Unité {unit.name}
          </Typography>
        </Grid>
        {beds.status === 'loading' ? (
          <>
            <Skeleton variant="circular" width={40} height={40} />
            <Skeleton variant="circular" width={40} height={40} />
          </>
        ) : display.printMode ? (
          <Grid key="checkbox">
            <Checkbox
              checked={isChecked}
              onChange={() => (isChecked ? dispatch(unselectBeds(busyBeds)) : dispatch(selectBeds(busyBeds)))}
              className={classes.infoIcon}
            />
          </Grid>
        ) : (
          <>
            <Grid>
              <DisplayedInfo
                title="Patients à gravité haute"
                icon={ToWatchIcon}
                value={computeNbSeverePatients(unitBeds)}
              />
            </Grid>
            <Grid>
              <DisplayedInfo title="Lits disponibles" icon={BedIcon} value={computeNbAvailableBeds(unitBeds)} />
            </Grid>
          </>
        )}
      </Grid>
    </Root>
  )
}

export default UnitTitle
