import React, { useEffect } from 'react'
import _ from 'lodash'

import List from '@mui/material/List'
import ListSubheader from '@mui/material/ListSubheader'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import Skeleton from '@mui/material/Skeleton'

import { classes, Root } from './styles'
import UnitTitle from './UnitTitle'
import Bed from '../Bed/Bed'
import { useAppDispatch, useAppSelector } from 'state'
import { fetchUnits } from 'state/units'

const Unit: React.FC<{ unitId: string }> = ({ unitId }) => {
  const dispatch = useAppDispatch()
  const { units } = useAppSelector((state) => ({
    units: state.units
  }))

  const unit = units.entities[unitId]

  useEffect(() => {
    if (!(unitId in units.entities) && units.status !== 'loading') {
      dispatch<any>(fetchUnits({ uuid: unitId }))
    }
  }, [])

  if (units.status === 'loading') {
    return <Skeleton variant="rounded" width={210} height={90} />
  }

  if (!unit) {
    return <Typography>Unité introuvable</Typography>
  }
  return (
    <Root>
      <Paper key={`unit-${unitId}`} className={classes.root}>
        <List
          className={classes.list}
          subheader={
            <ListSubheader component="div" className={classes.unitHeader}>
              <UnitTitle unit={unit} />
            </ListSubheader>
          }
        >
          {_.cloneDeep(unit.beds)?.map((bedId) => (
            <Bed key={`bed-${bedId}`} bedId={bedId} />
          ))}
        </List>
      </Paper>
    </Root>
  )
}

export default Unit
