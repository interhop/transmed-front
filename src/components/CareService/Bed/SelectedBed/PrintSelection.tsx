import React, { useEffect } from 'react'
import _ from 'lodash'
import { useDispatch } from 'react-redux'

import Checkbox from '@mui/material/Checkbox'
import Grid from '@mui/material/Unstable_Grid2'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import Skeleton from '@mui/material/Skeleton'
import Typography from '@mui/material/Typography'

import { classes, Root } from './styles'
import { FailuresIconsGrids, PatientDetailPrimary } from '../common'
import { dateToDayStep } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { selectBeds, unselectBeds } from 'state/selection'
import { fetchStays } from 'state/stays'
import { Bed as BedType, Patient } from 'types'

type ToPrintBedProps = {
  bed: BedType
}

const ToPrintBed: React.FC<ToPrintBedProps> = ({ bed }) => {
  const { selectedBeds, stays } = useAppSelector((state) => ({
    selectedBeds: state.selection.beds,
    stays: state.stays
  }))
  const dispatch = useDispatch<AppDispatch>()

  useEffect(() => {
    if (bed.currentPatientStay && !_.includes(stays.ids, bed.currentPatientStay) && stays.status !== 'loading') {
      dispatch<any>(fetchStays({ uuid: bed.currentPatientStay }))
    }
  }, [])

  if (!bed.currentPatientStay) return <></>
  const stay = stays.entities[bed.currentPatientStay]
  const patient: Patient = stay?.patient as Patient
  if (!stay && stays.status === 'loading') {
    return <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
  }
  if (!stay || !stay.patient) return <></>

  return (
    <Root>
      <ListItemButton
        key={bed.uuid}
        className={classes.bedItem}
        onClick={() =>
          _.includes(selectedBeds, bed.uuid) ? dispatch(unselectBeds([bed.uuid])) : dispatch(selectBeds([bed.uuid]))
        }
      >
        <ListItemIcon>
          <Checkbox checked={_.includes(selectedBeds, bed.uuid)} />
        </ListItemIcon>
        <Grid container alignItems="center" width="100%">
          <Grid container xs={6} sm={4} spacing={1} alignContent="center">
            {PatientDetailPrimary(patient)}
          </Grid>
          <Grid container xs={6} sm={4} className={classes.secondaryText}>
            <Typography
              variant={stay.hospitalisationCause && stay.hospitalisationCause.length > 30 ? 'body2' : 'body1'}
            >
              {dateToDayStep(stay.startDate)} - {stay.hospitalisationCause}
            </Typography>
          </Grid>
          <Grid container sm={4} className={classes.thirdDetails}>
            <FailuresIconsGrids
              onlyActivated
              readOnly
              failures={stay.failureMeasures}
              detections={stay.detections}
              stay={stay}
            />
          </Grid>
        </Grid>
      </ListItemButton>
    </Root>
  )
}

export default ToPrintBed
