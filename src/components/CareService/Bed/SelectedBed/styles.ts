import { styled } from '@mui/material/styles'

const PREFIX = 'SelectedBed'

export const classes = {
  bedItem: `${PREFIX}-bedItem`,
  patientDetails: `${PREFIX}-patientDetails`,
  thirdDetails: `${PREFIX}-thirdDetails`,
  secondaryText: `${PREFIX}-secondaryText`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.bedItem}`]: {
    height: theme.spacing(30),
    paddingRight: theme.spacing(4),
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      height: 'auto'
    }
  },
  [`& .${classes.thirdDetails}`]: {
    padding: 2,
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  [`& .${classes.secondaryText}`]: {
    color: theme.palette.text.disabled
  }
}))
