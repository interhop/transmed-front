import { styled } from '@mui/material/styles'

const PREFIX = 'DefaultBed'

export const classes = {
  bedItem: `${PREFIX}-bedItem`,
  highSeverity: `${PREFIX}-highSeverity`,
  mediumSeverity: `${PREFIX}-mediumSeverity`,
  bedIndex: `${PREFIX}-bedIndex`,
  thirdDetails: `${PREFIX}-thirdDetails`,
  additionalDetails: `${PREFIX}-additionalDetails`,
  actionIcon: `${PREFIX}-actionIcon`,
  rowIcon: `${PREFIX}-rowIcon`,
  todoButton: `${PREFIX}-todoButton`,
  disabled: `${PREFIX}-disabled`,
  info: `${PREFIX}-info`,
  action: `${PREFIX}-action`,
  text: `${PREFIX}-text`,
  secondaryText: `${PREFIX}-secondaryText`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.bedItem}`]: {
    height: theme.spacing(30),
    [theme.breakpoints.down('md')]: {
      height: 'auto',
      paddingRight: '66px !important'
    }
  },
  [`& .${classes.bedIndex}`]: {
    margin: theme.spacing(5),
    height: '2.5rem',
    width: '2rem'
  },
  [`& .${classes.highSeverity}`]: {
    border: `2px solid ${theme.palette.error.dark}`,
    backgroundColor: theme.palette.error.main,
    color: theme.palette.secondary.contrastText,
    borderRadius: 12
  },
  [`& .${classes.mediumSeverity}`]: {
    border: `2px solid ${theme.palette.warning.dark}`,
    backgroundColor: theme.palette.warning.main,
    color: theme.palette.secondary.contrastText,
    borderRadius: 12
  },
  [`& .${classes.thirdDetails}`]: {
    boxShadow: '0px 4px 3px rgba(0,0,0,0.15)',
    borderRadius: 6,
    padding: 2,
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  [`& .${classes.additionalDetails}`]: {
    // [theme.breakpoints.down('sm')]: {
    //   display: 'none'
    // }
  },
  [`& .${classes.actionIcon}`]: {
    margin: 'auto',
    width: 30,
    height: 30,
    color: theme.palette.secondary.main
  },
  [`& .${classes.text}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.secondaryText}`]: {
    color: theme.palette.text.disabled
  },
  [`& .${classes.rowIcon}`]: {
    aspectRatio: '1/1',
    height: '3rem',
    width: 'auto'
  },
  [`& .${classes.todoButton}`]: {
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  },
  [`& .${classes.disabled}`]: {
    color: theme.palette.text.disabled
  },
  [`& .${classes.info}`]: {
    color: theme.palette.primary.main
  },
  [`& .${classes.action}`]: {
    color: theme.palette.secondary.main
  }
}))
