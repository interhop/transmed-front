import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import CancelIcon from '@mui/icons-material/Cancel'
import Accordion from '@mui/material/Accordion'
import AccordionDetails from '@mui/material/AccordionDetails'
import AccordionSummary from '@mui/material/AccordionSummary'
import Grid from '@mui/material/Unstable_Grid2'
import Skeleton from '@mui/material/Skeleton'
import Typography from '@mui/material/Typography'

import { classes, Root } from './styles'
import { FailuresIconsGrids, PatientDetailPrimary } from '../common'
import { dateToDayStep } from 'assets/utils'
import CheckList from 'components/Basic/CheckList/CheckList'
import MyDialogActions from 'components/Dialogs/MyDialogActions/MyDialogActions'
import { AppDispatch, useAppSelector } from 'state'
import { editBedStayNote, fetchStay } from 'state/stays'
import { Bed as BedType, Patient, PatientNote, State } from 'types'

const defaultTodo: Partial<PatientNote> = {
  type: 'todo'
}

type TodoModeBedProps = {
  bed: BedType
}

const TodoModeBed: React.FC<TodoModeBedProps> = ({ bed }) => {
  const { stays } = useAppSelector((state) => state)
  const dispatch = useDispatch<AppDispatch>()

  const [editedData, setEditedData] = React.useState<Partial<PatientNote>>(defaultTodo)

  useEffect(() => {
    if (bed.currentPatientStay && !_.includes(stays.ids, bed.currentPatientStay) && stays.status !== 'loading') {
      dispatch<any>(fetchStay(bed.currentPatientStay))
    }
  }, [bed.currentPatientStay])

  // useEffect(() => {
  //   bed.currentPatientStay &&
  //     setEditedData(_.cloneDeep(stays.entities[bed.currentPatientStay]?.todoList ?? defaultTodo))
  // }, [stays.ids.length])

  const cancelEditDial = () => {
    setEditedData(defaultTodo)
  }

  const onChangeText = (content: string) => {
    setEditedData(Object.assign(_.cloneDeep(editedData), { content }))
  }

  const getTodoList = (): State<PatientNote> | undefined =>
    (stays.entities[bed.currentPatientStay ?? '']?.notes ?? []).find(_.matches({ type: 'todo' }))

  const onSubmit = () => {
    const todoId = getTodoList()?.uuid
    dispatch(
      todoId
        ? editBedStayNote({ newNote: _.pick(editedData, ['content']), oldNoteId: todoId })
        : editBedStayNote({ newNote: editedData })
    )
  }

  if (!bed.currentPatientStay) return <></>
  const stay = stays.entities[bed.currentPatientStay]
  const patient: Patient = stay?.patient as Patient
  if (!stay && stays.status === 'loading') {
    return <Skeleton variant="rounded" height={48} sx={{ margin: '2px' }} />
  }
  if (!stay || !stay.patient) return <></>

  const todoList = getTodoList()
  const displayedContent = editedData.content ?? todoList?.content ?? ''

  return (
    <Root>
      <Accordion key={bed.uuid} className={classes.accordion} defaultExpanded={!!displayedContent}>
        <AccordionSummary className={`${classes.bedItem} ${classes.expansionSummary} ${classes.text}`}>
          <Grid container alignItems="center" width="100%">
            <Grid
              container
              xs={1}
              className={`${classes.bedIndex} ${
                stay.severity === 0 ? classes.highSeverity : stay.severity === 1 ? classes.mediumSeverity : ''
              }`}
              justifyContent="center"
              alignItems="center"
            >
              {bed.unitIndex}
            </Grid>
            <Grid container xs={10}>
              <Grid container xs={6} sm={4} spacing={1} alignContent="center">
                {PatientDetailPrimary(patient)}
              </Grid>
              <Grid container xs={6} sm={4} className={classes.secondaryText}>
                <Typography
                  variant={stay.hospitalisationCause && stay.hospitalisationCause.length > 30 ? 'body2' : 'body1'}
                >
                  {dateToDayStep(stay.startDate)} - {stay.hospitalisationCause}
                </Typography>
              </Grid>
              <Grid container sm={4} className={classes.thirdDetails}>
                <FailuresIconsGrids
                  onlyActivated
                  readOnly
                  failures={stay.failureMeasures}
                  detections={stay.detections}
                  stay={stay}
                />
              </Grid>
            </Grid>
          </Grid>
        </AccordionSummary>
        <AccordionDetails>
          <CheckList text={displayedContent} onChange={onChangeText} />
          <br />
          {!!editedData.content && editedData.content != todoList?.content && (
            <MyDialogActions
              onCancel={cancelEditDial}
              CancelIcon={CancelIcon}
              onSubmit={onSubmit}
              submitLoading={todoList?.loading}
            />
          )}
        </AccordionDetails>
      </Accordion>
    </Root>
  )
}

export default TodoModeBed
