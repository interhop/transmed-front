import React from 'react'
import Snackbar from '@mui/material/Snackbar'
import MuiAlert from '@mui/lab/Alert'

import { AppDispatch, useAppSelector } from 'state'
import { clearMessage } from 'state/message'
import { useDispatch } from 'react-redux'

const SnackBar: React.FC<any> = () => {
  const { message } = useAppSelector((state) => ({
    message: state.message
  }))
  const dispatch = useDispatch<AppDispatch>()

  const clear = () => dispatch(clearMessage())

  return (
    <Snackbar
      open={!!message?.content}
      autoHideDuration={message?.type === 'success' ? 4000 : 10000}
      onClose={() => message?.type === 'success' && clear()}
    >
      <MuiAlert elevation={6} variant="filled" onClose={clear} severity={message?.type}>
        {message?.content}
      </MuiAlert>
    </Snackbar>
  )
}

export default SnackBar
