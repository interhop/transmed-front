import React, { useEffect } from 'react'
import BuiForm from '@rjsf/mui'
import validator from '@rjsf/validator-ajv8'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { Skeleton } from '@mui/material'

import { classes, Root } from './styles'
import { isSameDate, toISOLocale, toISOLocaleDate } from 'assets/utils'
import MyDialogActions from 'components/Dialogs/MyDialogActions/MyDialogActions'
import { NewAccessFormProps, newAccessFormSchema, uiNewAccessFormSchema } from 'components/formSchemas'
import { AppDispatch, useAppSelector } from 'state'
import { createAccess } from 'state/accesses'
import { fetchServices } from 'state/intensiveCareServices'
import { clearAccessForm } from 'state/modals'
import { Access, FrontInput, IntensiveCareService } from 'types'

const defaultFormData: Partial<Access> = {
  startDatetime: toISOLocaleDate(new Date())
}

const AccessForm: React.FC<any> = () => {
  const { me, services, accessForm } = useAppSelector((state) => ({
    me: state.me.user,
    services: state.intensiveCareServices,
    accessForm: state.modals.accessForm
  }))
  const dispatch = useDispatch<AppDispatch>()
  const [formData, setFormData] = React.useState<Partial<Access>>(defaultFormData)

  useEffect(() => {
    if (services.status !== 'loading' && services.ids.length === 0) dispatch(fetchServices())
  }, [])

  useEffect(() => {
    if (accessForm.accepted) {
      setFormData(defaultFormData)
      dispatch(clearAccessForm())
    }
  }, [accessForm.loading])

  const cancelDial = () => {
    setFormData({})
  }

  const adaptFormData = (data: NewAccessFormProps): NewAccessFormProps => {
    const newData = _.cloneDeep(data)
    if (newData.startDatetime && isSameDate(newData.startDatetime, new Date())) {
      newData.startDatetime = toISOLocale(_.now())
    }
    if (newData.endDatetime) {
      const end = new Date(newData.endDatetime)
      end.setHours(23)
      end.setMinutes(59)
      end.setSeconds(59)
      newData.endDatetime = toISOLocale(end)
    }
    return newData
  }

  const submit = () => {
    me &&
      dispatch(
        createAccess({
          user: me.uuid,
          rightEdit: true,
          ...adaptFormData(formData)
        } as FrontInput<Access>)
      )
  }

  if (services.status === 'loading') return <Skeleton variant="rounded" width="100%" height={200} />

  return services.ids.length ? (
    <Root className={classes.root}>
      <BuiForm
        schema={newAccessFormSchema(_.filter(_.values(services.entities)) as IntensiveCareService[])}
        uiSchema={uiNewAccessFormSchema}
        formData={_.cloneDeep(formData)}
        onChange={(form) => setFormData(form.formData)}
        liveValidate={false}
        validator={validator}
      >
        {!_.isEmpty(formData) && (
          <MyDialogActions
            onCancel={cancelDial}
            onSubmit={submit}
            submitActionTitle="Demander l'accès"
            disableSubmit={!formData.intensiveCareService}
            submitLoading={accessForm.loading}
          />
        )}
      </BuiForm>
    </Root>
  ) : (
    <></>
  )
}

export default AccessForm
