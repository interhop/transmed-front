import React from 'react'
import CircularProgress from '@mui/material/CircularProgress'
import IconButton from '@mui/material/IconButton'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction'
import ListItemText from '@mui/material/ListItemText'
import { SvgIconProps } from '@mui/material/SvgIcon'
import { Theme, useMediaQuery, useTheme } from '@mui/material'

import { classes, Root } from './styles'
import { dateTimeToStr, displayUser } from 'assets/utils'
import { Access as AccessType, IntensiveCareService, State, User } from 'types'

type AccessProps = {
  access: State<AccessType>
  user?: User
  service?: IntensiveCareService
  actions?: { callback: (i: string) => void; Icon: React.FC<SvgIconProps> }[]
}

const Access: React.FC<AccessProps> = ({ access, actions, user, service }) => {
  const th: Theme = useTheme()
  const smallScreen = useMediaQuery(th.breakpoints.down('sm'))

  return (
    <Root>
      <ListItemButton key={access.uuid} className={classes.accessItem}>
        {smallScreen ? (
          <ListItemText
            primary={`${displayUser(user)} : ${service?.hospitalName} - ${service?.name}`}
            secondary={`${dateTimeToStr(access.startDatetime)} - ${dateTimeToStr(access.endDatetime)}`}
            className={classes.accessDetails}
          />
        ) : (
          <>
            <ListItemText
              primary={displayUser(user)}
              secondary={`${service?.hospitalName} - ${service?.name}`}
              className={classes.accessDetails}
            />
            <ListItemText
              primary="Début"
              secondary={dateTimeToStr(access.startDatetime)}
              className={classes.dateDetail}
            />
            <ListItemText primary="Fin" secondary={dateTimeToStr(access.endDatetime)} className={classes.dateDetail} />
          </>
        )}
        {actions ? (
          <ListItemSecondaryAction>
            {actions.map(({ callback, Icon }, index) => (
              <IconButton
                key={`action-${index}`}
                onClick={() => !access.loading && callback(access.uuid)}
                disabled={access.loading}
              >
                {access.loading ? (
                  <CircularProgress className={classes.circularProgress} />
                ) : (
                  <Icon className={classes.icon} />
                )}
              </IconButton>
            ))}
          </ListItemSecondaryAction>
        ) : (
          <></>
        )}
      </ListItemButton>
    </Root>
  )
}

export default Access
