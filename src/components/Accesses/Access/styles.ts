import { styled } from '@mui/material/styles'

const PREFIX = 'AccessItem'
export const classes = {
  accessItem: `${PREFIX}-accessItem`,
  fullDetail: `${PREFIX}-fullDetail`,
  accessDetails: `${PREFIX}-accessDetails`,
  dateDetail: `${PREFIX}-dateDetail`,
  icon: `${PREFIX}-icon`,
  circularProgress: `${PREFIX}-circularProgress`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.accessItem}`]: {
    width: '100%',
    height: '60px',
    paddingRight: '166px !important',
    [theme.breakpoints.down('sm')]: {
      // height: '100px',
      paddingRight: '66px !important'
    }
  },
  [`& .${classes.accessDetails}`]: {
    width: '20%',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      maxWidth: '100%'
    }
  },
  [`& .${classes.dateDetail}`]: {
    width: '20%',
    [theme.breakpoints.down('sm')]: {
      width: '15%'
    }
  },
  [`& .${classes.icon}`]: {
    color: theme.palette.secondary.main,
    width: '30px',
    height: '100%',
    margin: '2px'
  },
  [`& .${classes.circularProgress}`]: {
    color: theme.palette.secondary.main
  }
}))
