import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'Severity'
export const classes = {
  rootBox: `${PREFIX}-rootBox`,
  boldHeader: `${PREFIX}-boldHeader`,
  infoCell: `${PREFIX}-infoCell`,
  infoText: `${PREFIX}-infoText`,
  severityIcon: `${PREFIX}-severityIcon`,
  highSeverityIcon: `${PREFIX}-highSeverityIcon`,
  mediumSeverityIcon: `${PREFIX}-mediumSeverityIcon`,
  lowSeverityIcon: `${PREFIX}-lowSeverityIcon`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.rootBox}`]: {
    padding: theme.spacing(3),
    margin: theme.spacing(3, 0, 3),
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    borderRadius: theme.spacing(3),
    boxShadow: '0px 4px 6px rgba(0,0,0,0.15)'
  },
  [`& .${classes.boldHeader}`]: {
    fontWeight: 'bold',
    marginRight: theme.spacing(3)
  },
  [`& .${classes.infoCell}`]: {
    margin: theme.spacing(2)
  },
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.severityIcon}`]: {
    width: 40,
    height: '100%',
    borderWidth: 1
  },
  [`& .${classes.highSeverityIcon}`]: {
    color: theme.palette.error.main,
    borderWidth: 5
  },
  [`& .${classes.mediumSeverityIcon}`]: {
    color: theme.palette.warning.main
  },
  [`& .${classes.lowSeverityIcon}`]: {
    color: theme.palette.primary.light
  }
}))
