import React from 'react'
import _ from 'lodash'

import { Box, Grid, MenuItem, Select, Skeleton } from '@mui/material'
import { SelectChangeEvent } from '@mui/material/Select/SelectInput'

import { classes, Root } from './styles'
import CardTitle from '../CardTitle/CardTitle'
import { ToWatchIcon } from 'assets/icons'
import { useAppDispatch, useAppSelector } from 'state'
import { updateStay } from 'state/stays'
import { SeverityType, Stay } from 'types'

const Severity: React.FC<{ stayId: string }> = ({ stayId }) => {
  const { stays, display } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings
  }))
  const dispatch = useAppDispatch()

  const stay = stayId in stays.entities ? stays.entities[stayId] : undefined

  const onChange: (event: SelectChangeEvent<any>) => void = ({ target: { value } }) => {
    if (display.todoMode) return
    let realValue = Number(value)
    realValue = (_.includes([0, 1, 2], realValue) ? realValue : 2) as Stay['severity']
    dispatch(
      updateStay({
        stayId,
        data: {
          severity: realValue as SeverityType
        }
      })
    )
  }

  const severityClassNames: { [Key in SeverityType]: string } = {
    0: classes.highSeverityIcon,
    1: classes.mediumSeverityIcon,
    2: classes.lowSeverityIcon
  }

  const severityLabels: { [Key in SeverityType]: string } = {
    0: 'Haute',
    1: 'Moyenne',
    2: 'Faible'
  }

  if (stays.status === 'loading') return <Skeleton variant="rounded" height={60} />
  if (!stay) return <>no stay in stay = stays.entities</>

  return (
    <Root>
      <Box className={classes.rootBox}>
        <Grid container alignItems="center" justifyContent="center">
          <CardTitle title="Gravité" />
          <Grid item container alignItems="center" justifyContent="space-evenly">
            <ToWatchIcon className={`${classes.severityIcon} ${severityClassNames[stay.severity ?? 2]}`} />

            <Select
              margin="dense"
              onChange={onChange}
              className={classes.infoCell}
              value={stay.severity}
              variant="outlined"
              disabled={display.todoMode}
            >
              {Object.entries(severityLabels).map(([value, displayed]) => (
                <MenuItem key={value} value={value}>
                  {displayed}
                </MenuItem>
              ))}
            </Select>
          </Grid>
        </Grid>
      </Box>
    </Root>
  )
}

export default Severity
