import React, { useEffect, useState } from 'react'
import _ from 'lodash'

import { CircularProgress, Unstable_Grid2 as Grid, IconButton, Skeleton, Tooltip, Typography } from '@mui/material'
import {
  ArrowBackIos as ArrowBackIosIcon,
  ArrowForwardIos as ArrowForwardIosIcon,
  Edit as EditIcon
} from '@mui/icons-material'

import { classes, Root } from './styles'
import CardTitle from '../CardTitle/CardTitle'
import Markdown from 'components/Basic/Markdown/Markdown'
import { dateTimeToStr, displayUser } from 'assets/utils'
import { useAppDispatch, useAppSelector } from 'state'
import { openMarkdownDial } from 'state/modals'
import { fetchUsers } from 'state/users'
import { FrontInput, PatientNote, Stay, User } from 'types'

const batchNote = (stay?: Stay, user?: User | null): FrontInput<PatientNote> => ({
  createdBy: user ? user.uuid : undefined,
  stay: stay?.uuid,
  type: 'general',
  content: ''
})

const getGeneralNotes = (stay?: Stay): PatientNote[] =>
  (stay?.notes || []).filter(_.matches<Partial<PatientNote>>({ type: 'general' }))

const getMyNote = (notes: FrontInput<PatientNote>[], userId: string): FrontInput<PatientNote> | undefined =>
  notes.find(_.matches<Partial<PatientNote>>({ createdBy: userId }))

export const GeneralNotes: React.FC<{ stayId: string }> = ({ stayId }) => {
  const { stays, display, me, users } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings,
    me: state.me.user,
    users: state.users
  }))
  const dispatch = useAppDispatch()
  const [currentNote, setCurrentNote] = useState<FrontInput<PatientNote>>(batchNote())

  const stay = stays.entities[stayId]

  useEffect(() => {
    if (!me || !_.includes(stays.ids, stayId) || stays.status === 'loading') return
    const stay = stays.entities[stayId]
    const notes = getGeneralNotes(stay)
    const mostRecentNote = _.maxBy(notes, (n) => new Date(n.createdAt ?? '').valueOf())

    const note = getMyNote(notes, me.uuid) ?? mostRecentNote ?? batchNote(stay, me)
    setCurrentNote(note)
  }, [stays, stayId])

  useEffect(() => {
    if (!_.includes(stays.ids, stayId)) return
    if (!users.ids.length && users.status !== 'loading') {
      dispatch(fetchUsers({ authorizedReanimationServices__unit__bed: stays.entities[stayId]?.bed }))
    }
  }, [stays])

  const getNextNote = (): FrontInput<PatientNote> | undefined => {
    const genNotes: FrontInput<PatientNote>[] = getGeneralNotes(stay)
    if (!getMyNote(genNotes, me?.uuid ?? '')) genNotes.push(batchNote(stay, me))
    if (genNotes.length < 2) return
    if (!currentNote.uuid) return genNotes.at(0)
    return (
      genNotes.at(genNotes.findIndex(_.matches<Partial<PatientNote>>({ uuid: currentNote.uuid })) + 1) ?? genNotes.at(0)
    )
  }

  const getPreviousNote = (): FrontInput<PatientNote> | undefined => {
    const genNotes: FrontInput<PatientNote>[] = getGeneralNotes(stay)
    if (!getMyNote(genNotes, me?.uuid ?? '')) genNotes.unshift(batchNote(stay, me))
    if (genNotes.length < 2) return
    if (!currentNote.uuid) return genNotes.at(-1)
    return genNotes.at(genNotes.findIndex(_.matches<Partial<PatientNote>>({ uuid: currentNote.uuid })) - 1)
  }

  const nextNote = getNextNote()
  const previousNote = getPreviousNote()

  const openDial = () => {
    if (display.todoMode) return
    if (!me || (currentNote.uuid && currentNote.createdBy !== me.uuid)) return
    dispatch(openMarkdownDial({ note: currentNote }))
  }

  if (stays.status === 'loading') return <Skeleton variant="rounded" height={500} />
  if (!currentNote) return <></>
  if (!me) return <></>

  return (
    <Root>
      <Grid container spacing={2} justifyContent="space-evenly">
        <Grid container sm={12} justifyContent="space-evenly">
          <Typography variant="h6" className={`${classes.title} ${classes.infoText}`}>
            Notes générales
          </Typography>
        </Grid>
      </Grid>
      <Grid container className={classes.card} spacing={2} justifyContent="space-evenly">
        {previousNote && (
          <Tooltip
            title={
              users.status === 'loading' ? (
                <CircularProgress size={12} className={classes.buttonProgress} />
              ) : (
                displayUser(users.entities[previousNote.createdBy ?? ''])
              )
            }
          >
            <IconButton onClick={() => setCurrentNote(previousNote)}>
              <ArrowBackIosIcon className={classes.icon} />
            </IconButton>
          </Tooltip>
        )}
        <CardTitle
          title={
            !currentNote.uuid || currentNote.createdBy === me.uuid
              ? displayUser(me)
              : displayUser(users.entities[currentNote.createdBy ?? ''])
          }
          canEdit={!display.todoMode && (!currentNote.uuid || currentNote.createdBy === me.uuid)}
          onClick={openDial}
          Icon={EditIcon}
        />
        {nextNote && (
          <Tooltip
            title={
              users.status === 'loading' ? (
                <CircularProgress size={12} className={classes.buttonProgress} />
              ) : (
                displayUser(users.entities[nextNote.createdBy ?? ''])
              )
            }
          >
            <IconButton onClick={() => setCurrentNote(nextNote)}>
              <ArrowForwardIosIcon className={classes.icon} />
            </IconButton>
          </Tooltip>
        )}
        {currentNote.content && (
          <Grid container onDoubleClick={openDial} justifyContent="flex-start">
            <Grid xs={12}>
              <Typography variant="body2">Mise à jour le {dateTimeToStr(currentNote.updatedAt)}</Typography>
            </Grid>
            <Markdown content={currentNote.content} />
          </Grid>
        )}
      </Grid>
    </Root>
  )
}

export default GeneralNotes
