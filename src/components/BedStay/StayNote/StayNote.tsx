import React from 'react'
import _ from 'lodash'

import { Unstable_Grid2 as Grid, Skeleton, Typography } from '@mui/material'
import EditIcon from '@mui/icons-material/Edit'

import { classes, Root } from './styles'
import CardTitle from '../CardTitle/CardTitle'
import Markdown from 'components/Basic/Markdown/Markdown'
import { dateTimeToStr } from 'assets/utils'
import { useAppDispatch, useAppSelector } from 'state'
import { openMarkdownDial } from 'state/modals'
import { FrontInput, PatientNote, PatientNoteType, Stay } from 'types'

const batchNote = (stay: Stay, type: PatientNoteType): FrontInput<PatientNote> => ({
  stay: stay.uuid,
  type,
  content: ''
})

const getNote = (stay: Stay, type: PatientNoteType): FrontInput<PatientNote> =>
  (stay.notes ?? []).find(_.matches<Partial<PatientNote>>({ type })) ?? batchNote(stay, type)

type StayNoteProps = {
  stayId: string
  type: PatientNoteType
}

const titles: { [Key in PatientNoteType]: string } = {
  evolution: 'Evolution',
  rdh: 'Histoire de la maladie récente',
  general: 'Notes générales',
  todo: 'Todo list',
  dn: 'Notes quotidiennes',
  tl: ''
}

export const StayNote: React.FC<StayNoteProps> = ({ stayId, type }) => {
  const { stays, display } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings
  }))
  const dispatch = useAppDispatch()
  const stay = stays.entities[stayId]

  const openDial = () => {
    if (display.todoMode) return
    if (!stay) return
    dispatch(openMarkdownDial({ title: titles[type], note: getNote(stay, type) ?? batchNote(stay, type) }))
  }

  if (stays.status === 'loading') return <Skeleton variant="rounded" height={250} />
  if (!stay) return <></>

  const note = getNote(stay, type)
  return (
    <Root>
      <Grid container spacing={2} justifyContent="center" className={classes.card}>
        <CardTitle title={titles[type]} canEdit={!display.todoMode} onClick={openDial} Icon={EditIcon} />
        {note.content && (
          <Grid container sx={{ width: '100%' }} onDoubleClick={openDial} justifyContent="flex-start">
            <Grid xs={12}>
              <Typography variant="body2">Mise à jour le {dateTimeToStr(note.updatedAt)}</Typography>
            </Grid>
            <Markdown content={note.content} />
          </Grid>
        )}
      </Grid>
    </Root>
  )
}

export default StayNote
