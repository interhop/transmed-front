import { styled } from '@mui/material/styles'

const PREFIX = 'CardTitle'
export const classes = {
  infoText: `${PREFIX}-infoText`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  }
}))
