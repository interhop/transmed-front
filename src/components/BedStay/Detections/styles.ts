import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'Detections'
export const classes = {
  rootBox: `${PREFIX}-rootBox`,
  boldHeader: `${PREFIX}-boldHeader`,
  italicComplement: `${PREFIX}-italicComplement`,
  infoText: `${PREFIX}-infoText`,
  cellBox: `${PREFIX}-cellBox`,
  title: `${PREFIX}-title`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.rootBox}`]: {
    padding: theme.spacing(3),
    margin: theme.spacing(3, 0, 3),
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    borderRadius: 6,
    boxShadow: '0px 4px 6px rgba(0,0,0,0.15)'
  },
  [`& .${classes.boldHeader}`]: {
    fontWeight: 'bold',
    marginRight: theme.spacing(3)
  },
  [`& .${classes.italicComplement}`]: {
    fontStyle: 'italic',
    marginLeft: theme.spacing(3)
  },
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.cellBox}`]: {
    margin: theme.spacing(1),
    padding: theme.spacing()
  },
  [`& .${classes.title}`]: {
    margin: theme.spacing(5),
    width: '100%',
    textAlign: 'center'
  }
}))
