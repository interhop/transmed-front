import React from 'react'
import _ from 'lodash'

import { Box, Grid, Skeleton, Typography } from '@mui/material'
import AddIcon from '@mui/icons-material/AddCircle'

import { classes, Root } from './styles'
import CardTitle from '../CardTitle/CardTitle'
import DetectionDial from 'components/Dialogs/DetectionsDial'
import { dateTimeToStr, nbDaysBetween } from 'assets/utils'
import { useAppDispatch, useAppSelector } from 'state'
import { openDetectionAdd } from 'state/modals'
import { PatientDetection } from 'types'

export const detectionsToInfos = (detections: PatientDetection[]): { date: string; label: string }[] => {
  const sortedDetections = _.cloneDeep(detections)
  sortedDetections.sort((b, a) => new Date(a.measureDatetime).valueOf() - new Date(b.measureDatetime).valueOf())

  const getLastDetection = (field: keyof PatientDetection): PatientDetection | undefined => {
    return sortedDetections.find(_.matches({ [field]: true }))
  }

  const infos: { date: string; label: string }[] = []

  infos.push(
    ...[
      {
        date: dateTimeToStr(getLastDetection('blse')?.measureDatetime),
        label: 'BLSE'
      },
      {
        date: dateTimeToStr(getLastDetection('sarm')?.measureDatetime),
        label: 'SARM'
      },
      {
        date: dateTimeToStr(getLastDetection('bhc')?.measureDatetime),
        label: 'BHC'
      },
      {
        date: dateTimeToStr(getLastDetection('clostridium')?.measureDatetime),
        label: 'Clostridium'
      }
    ]
  )
  const lastNeg = sortedDetections.find(_.matches({ bhc: false, blse: false, sarm: false, clostridium: false }))
  lastNeg &&
    infos.push({
      date: dateTimeToStr(lastNeg?.measureDatetime),
      label: 'Dernier test négatif'
    })
  infos.sort((a, b) => (a.date && b.date ? new Date(a.date).valueOf() - new Date(b.date).valueOf() : !b.date ? 1 : -1))

  return infos
}

export const Detections: React.FC<{ stayId: string }> = ({ stayId }) => {
  const { stays, display } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings
  }))
  const dispatch = useAppDispatch()

  const stay = stayId in stays.entities ? stays.entities[stayId] : undefined

  const detections: PatientDetection[] = _.cloneDeep(stay?.detections || [])

  const infos = detectionsToInfos(detections)

  const sinceLastDetection = nbDaysBetween(
    _.maxBy(detections, ({ measureDatetime }) => new Date(measureDatetime).valueOf())?.measureDatetime,
    _.now()
  )

  if (stays.status === 'loading') return <Skeleton variant="rounded" height={150} />
  if (!stay) return <>no stay: stay = stays.entities</>

  return (
    <Root>
      <Box className={classes.rootBox}>
        <Grid container spacing={2} justifyContent="space-evenly">
          <CardTitle
            title="Dépistages"
            canEdit={!display.todoMode}
            onClick={() => dispatch(openDetectionAdd(stay))}
            Icon={AddIcon}
          />
          {sinceLastDetection >= 7 ? (
            <Typography variant="body1" className={`${classes.title} ${classes.infoText}`}>
              {sinceLastDetection} jours sans dépistage
            </Typography>
          ) : (
            <></>
          )}

          {infos.map(
            ({ date, label }) =>
              date && (
                <Grid item xs={12} sm={12} key={label}>
                  <Box className={classes.cellBox}>
                    <Grid container spacing={1}>
                      <Typography variant="body1" className={`${classes.boldHeader} ${classes.infoText}`}>
                        {label}:{' '}
                      </Typography>
                      <Typography variant="body1" className={classes.infoText}>
                        {date}
                      </Typography>
                    </Grid>
                  </Box>
                </Grid>
              )
          )}
        </Grid>
      </Box>
      <DetectionDial />
    </Root>
  )
}

export default Detections
