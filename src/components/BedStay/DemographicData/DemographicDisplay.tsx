import React, { ChangeEvent, useState } from 'react'
import _ from 'lodash'

import Button from '@mui/material/Button'
import Grid from '@mui/material/Unstable_Grid2'
import Skeleton from '@mui/material/Skeleton'
import Typography from '@mui/material/Typography'
import { InputProps } from '@mui/material/Input'
import FormControl from '@mui/material/FormControl'
import InputLabel from '@mui/material/InputLabel'
import Input from '@mui/material/Input'
import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'

import { History as HistoryIcon, Save as SaveIcon } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'

import { classes, Root } from './styles'
import CardTitle from '../CardTitle/CardTitle'
import { dateToDayStep, getAge, getUpdated, toISOLocaleDate } from 'assets/utils'
import { useAppDispatch, useAppSelector } from 'state'
import { updatePatient, updateStay } from 'state/stays'
import { Patient, PatientSex, Stay } from 'types'

//TOOD should be automated using jsonSchema but not that simple
const checkAddPatientForm = (field: keyof (Patient & Stay), value: string) => {
  switch (field) {
    case 'localId':
      return /^[0-9]*$/.test(value)
    case 'startDate':
      return new Date(value) <= new Date()
    case 'lastName':
      return /^[A-zÀ-ÖØ-öø-ÿ-' ]*$/.test(value)
    case 'firstName':
      return /^[A-zÀ-ÖØ-öø-ÿ-' ]*$/.test(value)
    case 'birthDate':
      return new Date(value) <= new Date()
    case 'weightKg':
      return /^[0-9]{0,3}(\.[0-9]{0,3})?$/.test(value)
    case 'sizeCm':
      return /^[0-9]{0,3}(\.[0-9]{0,2})?$/.test(value)
    default:
      return true
  }
}

type DemogInputProps = Partial<InputProps> & {
  label?: string
  changed: boolean
}

const DemogInput: React.FC<DemogInputProps> = (props) => (
  <FormControl color="secondary" fullWidth variant="outlined" className={classes.input}>
    {props.label && <InputLabel className={classes.inputLabel}>{props.label}</InputLabel>}
    <Input
      required
      componentsProps={{
        input: {
          className: props.type === 'number' ? classes.numberInput : props.type === 'date' ? classes.dateInput : ''
        }
      }}
      {...props}
      color={props.changed ? 'secondary' : 'primary'}
    />
  </FormControl>
)

const sexInterface: { [Key in PatientSex]: string } = {
  w: 'Femme',
  u: 'Non défini',
  m: 'Homme'
}

const imc = (p: Partial<Patient>): string | undefined => {
  if (!p.sizeCm || !p.weightKg) return
  return `(IMC: 
      ${p.sizeCm && p.weightKg ? (p.weightKg / (p.sizeCm / 100) ** 2).toFixed(2) : '?'})`
}

const DemographicDisplay: React.FC<{ stayId: string }> = ({ stayId }) => {
  const { stays, display } = useAppSelector((state) => ({
    stays: state.stays,
    display: state.settings
  }))
  const dispatch = useAppDispatch()

  const stay = stayId in stays.entities ? stays.entities[stayId] : undefined
  const patient = stay ? (stay.patient as Patient) : undefined

  const [editedStay, setEditedStay] = useState<Partial<Stay>>({})
  const [editedPatient, setEditedPatient] = useState<Partial<Patient>>({})

  // useEffect(() => {
  //   const s = stayId in stays.entities ? stays.entities[stayId] : undefined
  //   s && setEditedStay(_.cloneDeep(s))
  //   const p = s ? (s.patient as Patient) : undefined
  //   p && setEditedPatient(_.cloneDeep(p))
  // }, [stays])

  // const handleKeyPress: KeyboardEventHandler = (event) => {
  //   if (readOnly) return
  //   switch (event.key) {
  //     case 'Enter':
  //       onSubmit()
  //       break
  //     case 'Escape':
  //       onCancel()
  //       break
  //     default:
  //       return
  //   }
  // }

  const onCancel = () => {
    setEditedPatient({})
    setEditedStay({})
  }

  const onFieldChange = (field: keyof (Patient & Stay)) => {
    return (event: ChangeEvent<HTMLInputElement>): void => {
      if (!checkAddPatientForm(field, event.currentTarget.value.toString())) return

      if (_.includes(['startDate', 'hospitalisationCause'], field)) {
        setEditedStay({ ...editedStay, [field]: event.currentTarget.value })
      } else {
        setEditedPatient({ ...editedPatient, [field]: event.currentTarget.value })
      }
    }
  }

  const onSubmit = () => {
    if (!stay || !patient) return

    const stayDifferences = getUpdated(stay, editedStay || {})
    if (_.values(stayDifferences).length) {
      dispatch(updateStay({ stayId: stay.uuid, data: stayDifferences }))
    }

    const patientDifferences = getUpdated(patient, editedPatient || {})
    if (_.values(patientDifferences).length) {
      dispatch(updatePatient({ patientId: patient.uuid, data: patientDifferences }))
    }
  }

  const baseProps = (field: keyof (Patient & Stay)) => ({
    disabled: display.todoMode,
    onChange: onFieldChange(field),
    // onKeyDown: handleKeyPress,
    type: 'string',
    changed:
      (!!patient &&
        field in editedPatient &&
        editedPatient[field as keyof Patient] !== patient[field as keyof Patient]) ||
      (!!stay && field in editedStay && editedStay[field as keyof Stay] !== stay[field as keyof Stay])
  })

  if (stays.status === 'loading') return <Skeleton variant="rounded" height={300} />
  if (!stay) return <>no stay in stays.entities</>
  if (!patient) return <>no stay patient ({stay.patient})</>

  // todo : check out refactoring using <form>
  return (
    <Root>
      <Grid container spacing={2} justifyContent="space-evenly" className={classes.rootBox}>
        <Grid container sm={12}>
          <Grid xs={12} container justifyContent="center">
            <CardTitle title="Séjour" />
          </Grid>
          <Grid sm={5} xs={12}>
            <DemogInput
              label="Arrivée"
              {...baseProps('startDate')}
              value={toISOLocaleDate(editedStay?.startDate ?? stay.startDate)}
              type="date"
              endAdornment={
                <Typography variant="body1" sx={{ minWidth: '3rem' }}>
                  ({dateToDayStep(editedStay?.startDate ?? stay.startDate)})
                </Typography>
              }
            />
          </Grid>
          <Grid xs={12}>
            <DemogInput
              label="Motif d'admission"
              {...baseProps('hospitalisationCause')}
              value={editedStay?.hospitalisationCause ?? stay.hospitalisationCause}
              multiline
            />
          </Grid>
        </Grid>

        <Grid sm={12}>
          <Grid xs={12} container justifyContent="center">
            <CardTitle title="Informations démographiques" />
          </Grid>
          <Grid xs={12} container spacing={2} justifyContent="space-between">
            <Grid sm={7} xs={6}>
              <DemogInput
                label="Identifiant"
                {...baseProps('localId')}
                value={editedPatient?.localId ?? patient.localId}
              />
            </Grid>
            <Grid sm={6} xs={12}>
              <DemogInput label="Nom" {...baseProps('lastName')} value={editedPatient?.lastName ?? patient.lastName} />
            </Grid>
            <Grid sm={6} xs={12}>
              <DemogInput
                label="Prénom"
                {...baseProps('firstName')}
                value={editedPatient?.firstName ?? patient.firstName}
              />
            </Grid>
            <Grid sm={6}>
              <Select
                margin="dense"
                variant="outlined"
                label="Sexe"
                value={editedPatient?.sex ?? patient.sex}
                style={{ margin: '4px' }}
                className={classes.infoText}
                {...baseProps('sex')}
                onChange={({ target }) => setEditedPatient({ ...editedStay, sex: target.value as PatientSex })}
                color={baseProps('sex').changed ? 'secondary' : 'primary'}
              >
                {_.entries(sexInterface).map(([value, name]) => (
                  <MenuItem key={name} value={value} className={classes.infoText}>
                    {name}
                  </MenuItem>
                ))}
              </Select>
            </Grid>

            <Grid>
              <DemogInput
                label="Naissance"
                {...baseProps('birthDate')}
                type="date"
                value={editedPatient?.birthDate ?? patient.birthDate}
                endAdornment={
                  <Typography variant="body1" sx={{ minWidth: '4rem' }}>
                    ({getAge(editedPatient?.birthDate ?? patient.birthDate) || '?'} ans)
                  </Typography>
                }
              />
            </Grid>
            <Grid>
              <DemogInput
                label="Poids (kg)"
                {...baseProps('weightKg')}
                type="number"
                value={editedPatient?.weightKg ?? patient.weightKg}
              />
            </Grid>
            <Grid>
              <DemogInput
                label="Taille (cm)"
                {...baseProps('sizeCm')}
                type="number"
                value={editedPatient?.sizeCm ?? patient.sizeCm}
                endAdornment={
                  <Typography variant="body1" sx={{ minWidth: '5rem' }}>
                    {imc({ ...patient, ...editedPatient })}
                  </Typography>
                }
              />
            </Grid>
          </Grid>
        </Grid>
        {(_.entries(editedPatient).some(([k, v]) => !!v && v !== patient[k as keyof Patient]) ||
          _.entries(editedStay).some(([k, v]) => !!v && v !== stay[k as keyof Stay])) && (
          <Grid container justifyContent="center">
            <Button
              color="secondary"
              variant="outlined"
              startIcon={<HistoryIcon />}
              style={{ margin: '2px' }}
              onClick={onCancel}
            >
              Rétablir
            </Button>
            <LoadingButton
              color="secondary"
              variant="contained"
              startIcon={<SaveIcon />}
              style={{ margin: '2px' }}
              onClick={onSubmit}
              loading={stays.patientStatus === 'loading'}
            >
              Enregistrer
            </LoadingButton>
          </Grid>
        )}
      </Grid>
    </Root>
  )
}

export default DemographicDisplay
