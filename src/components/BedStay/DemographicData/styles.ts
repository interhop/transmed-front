import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'DemogData'
export const classes = {
  rootBox: `${PREFIX}-rootBox`,
  boldHeader: `${PREFIX}-boldHeader`,
  italicComplement: `${PREFIX}-italicComplement`,
  infoText: `${PREFIX}-infoText`,
  cellBox: `${PREFIX}-cellBox`,
  title: `${PREFIX}-title`,
  input: `${PREFIX}-input`,
  numberInput: `${PREFIX}-numberInput`,
  dateInput: `${PREFIX}-dateInput`,
  inputLabel: `${PREFIX}-inputLabel`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.rootBox}`]: {
    padding: '5px',
    margin: '5px',
    // backgroundColor: alpha(theme.palette.background.paper, 0.1),
    backgroundColor: alpha(theme.palette.background.paper, 0.9),
    borderRadius: '6px',
    boxShadow: '0px 4px 6px rgba(0,0,0,0.15)'
  },
  [`& .${classes.boldHeader}`]: {
    fontWeight: 'bold',
    marginRight: '5px'
  },
  [`& .${classes.italicComplement}`]: {
    fontStyle: 'italic',
    marginLeft: '5px'
  },
  [`& .${classes.infoText}`]: {
    color: theme.palette.text.primary
  },
  [`& .${classes.cellBox}`]: {
    margin: '2px',
    padding: '2px'
  },
  [`& .${classes.title}`]: {
    boxShadow: '0px 2px 3px rgba(0,0,0,0.15)',
    margin: '10px',
    textAlign: 'center'
  },
  [`& .${classes.input}`]: {
    margin: theme.spacing(2, 0, 2),
    fontSize: '1rem'
  },
  [`& .${classes.numberInput}`]: {
    maxWidth: '5rem'
  },
  [`& .${classes.dateInput}`]: {
    maxWidth: '8rem'
  },
  [`& .${classes.inputLabel}`]: {
    opacity: 0.5,
    fontSize: '1rem',
    [`&.Mui-focused`]: {
      color: theme.palette.secondary.dark,
      opacity: 1
    }
  }
}))
