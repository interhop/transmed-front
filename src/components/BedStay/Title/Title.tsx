import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import _ from 'lodash'

import {
  Add as AddIcon,
  ArrowBackIos as ArrowBackIosIcon,
  ArrowForwardIos as ArrowForwardIosIcon
} from '@mui/icons-material'
import Grid from '@mui/material/Unstable_Grid2'
import IconButton from '@mui/material/IconButton'
import Skeleton from '@mui/material/Skeleton'
import Tooltip from '@mui/material/Tooltip'
import Typography from '@mui/material/Typography'
import Button from '@mui/material/Button'
import { Theme, useMediaQuery, useTheme } from '@mui/material'

import { classes, Root } from './styles'
import LatIconButton from '../TreatmentLimitationsDial/LatIconButton'
import TreatmentLimitationsDialog from '../../Dialogs/TreatmentLimitationsDial'
import TodoIconButton from '../TodoList/TodoIconButton'
import RoutesConfig from 'components/Routes/AppNavigation/config'
import { displayName } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { openBedUpdate } from 'state/modals'
import { fetchUnits } from 'state/units'
import { Bed } from 'types'

type BedTitleProps = {
  bed: Bed
}

const BedTitle: React.FC<BedTitleProps> = ({ bed }) => {
  const th: Theme = useTheme()
  const smallScreen = useMediaQuery(th.breakpoints.down('sm'))
  const navigate = useNavigate()

  const dispatch = useDispatch<AppDispatch>()
  const { units, beds, stays } = useAppSelector((state) => ({
    beds: state.beds,
    units: state.units,
    stays: state.stays.entities
  }))

  const unit = units.entities[bed.unit]

  const getNextBed = (): Bed | undefined => {
    if (!unit || !unit.beds) return undefined
    const unitBeds = _.values(beds.entities).filter(_.matches({ unit: unit.uuid })) as Bed[]

    const maxUnitIndex = _.max(unitBeds.map((b) => b?.unitIndex))
    if (bed.unitIndex !== maxUnitIndex) {
      return unitBeds.find(_.matches({ unitIndex: bed.unitIndex + 1 }))
    } else {
      const nextUnitIdIndex = units.ids.findIndex(_.matches(unit.uuid)) + 1
      const nextUnitId = units.ids.at(nextUnitIdIndex) || units.ids.at(0)
      const nextUnit = units.entities[nextUnitId]
      if (!nextUnit) return

      const nextUnitBeds = _.values(beds.entities).filter(_.matches({ unit: nextUnit.uuid })) as Bed[]
      const minNextUnitIndex = _.min(nextUnitBeds.map((b) => b.unitIndex))
      return (nextUnitBeds ?? []).find(_.matches({ unitIndex: minNextUnitIndex }))
    }
  }

  const getPreviousBed = (): Bed | undefined => {
    if (!unit || !unit.beds) return undefined
    const unitBeds = _.values(beds.entities).filter(_.matches({ unit: unit.uuid })) as Bed[]

    const minUnitIndex = _.min(unitBeds.map((b) => b.unitIndex))
    if (bed.unitIndex !== minUnitIndex) {
      return unitBeds.find(_.matches({ unitIndex: bed.unitIndex - 1 }))
    } else {
      const prevUnitIdIndex = units.ids.findIndex(_.matches(unit.uuid)) - 1
      const prevUnitId = units.ids.at(prevUnitIdIndex)
      const prevUnit = units.entities[prevUnitId]
      if (!prevUnit) return
      const prevUnitBeds = _.values(beds.entities).filter(_.matches({ unit: prevUnit.uuid })) as Bed[]
      const maxPrevUnitIndex = _.max((prevUnitBeds ?? []).map((b) => b.unitIndex))
      return (prevUnitBeds ?? []).find(_.matches({ unitIndex: maxPrevUnitIndex }))
    }
  }

  const nextBed = getNextBed()
  const previousBed = getPreviousBed()

  useEffect(() => {
    if (!(bed.unit in units.entities) && units.status !== 'loading') {
      dispatch(fetchUnits({}))
    }
  }, [bed])

  if (beds.status === 'loading' || units.status === 'loading')
    return <Skeleton style={{ margin: 5 }} width="80%" height={190} />
  if (!unit) return <Typography>Unité introuvable</Typography>

  return (
    <Root>
      <Grid container className={classes.root} justifyContent="center">
        <Grid xs={12} container justifyContent="center">
          <Typography
            className={`${classes.unitCard}`}
            align="center"
            variant={smallScreen ? 'h5' : 'h2'}
            color="textSecondary"
            sx={{ cursor: 'pointer' }}
            onClick={() => navigate(RoutesConfig.intensiveCareService.getPath(unit.intensiveCareService))}
          >
            Unité {unit.name}
          </Typography>
        </Grid>
        <Grid xs={12} container justifyContent="center">
          <Grid container xs={3} alignContent="center">
            {previousBed && (
              <Grid xs={6}>
                <IconButton color="secondary" onClick={() => navigate(RoutesConfig.bed.getPath(previousBed.uuid))}>
                  <Tooltip title={previousBed?.bedDescription || ''}>
                    <ArrowBackIosIcon className={classes.icon} />
                  </Tooltip>
                </IconButton>
              </Grid>
            )}
            <Grid xs={6}>
              {bed.currentPatientStay && (
                <TodoIconButton
                  stayId={bed.currentPatientStay}
                  buttonProps={{ className: classes.button }}
                  className={classes.buttonIcon}
                />
              )}
            </Grid>
          </Grid>

          <Grid xs={5} className={classes.card}>
            {bed.currentPatientStay ? (
              <Typography align="center" variant={smallScreen ? 'h6' : 'h3'}>
                {bed.unitIndex} - {displayName(stays[bed.currentPatientStay]?.patient)}
              </Typography>
            ) : (
              <Button
                color="secondary"
                variant="contained"
                fullWidth
                endIcon={<AddIcon />}
                onClick={() => dispatch(openBedUpdate({ bed }))}
              >
                {bed.unitIndex} - Ajouter un patient
              </Button>
            )}
          </Grid>

          <Grid container xs={3} alignContent="center">
            <Grid xs={6}>
              {bed.currentPatientStay && (
                <LatIconButton
                  stayId={bed.currentPatientStay}
                  buttonProps={{ className: classes.button }}
                  className={classes.buttonIcon}
                />
              )}
            </Grid>
            {nextBed && (
              <Grid xs={6}>
                <Tooltip title={nextBed?.bedDescription || ''}>
                  <IconButton onClick={() => navigate(RoutesConfig.bed.getPath(nextBed.uuid))}>
                    <ArrowForwardIosIcon className={classes.icon} />
                  </IconButton>
                </Tooltip>
              </Grid>
            )}
          </Grid>
        </Grid>
        <TreatmentLimitationsDialog />
      </Grid>
    </Root>
  )
}

export default BedTitle
