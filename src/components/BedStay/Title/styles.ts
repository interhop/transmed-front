import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'BedStayTitle'
export const classes = {
  root: `${PREFIX}-root`,
  unitCard: `${PREFIX}-unitCard`,
  card: `${PREFIX}-card`,
  button: `${PREFIX}-button`,
  buttonIcon: `${PREFIX}-buttonIcon`,
  icon: `${PREFIX}-icon`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.root}`]: {
    padding: '1rem',
    width: '100%',
    margin: 5,
    [theme.breakpoints.down('sm')]: {
      padding: 0
    }
  },
  [`& .${classes.unitCard}`]: {
    backgroundColor: alpha(theme.palette.secondary.main, 0.6),
    color: theme.palette.secondary.contrastText,
    borderRadius: 12,
    margin: '0.5rem',
    padding: '1rem',
    boxShadow: '0px 8px 12px rgba(0,0,0,0.15)'
  },
  [`& .${classes.card}`]: {
    backgroundColor: alpha(theme.palette.background.paper, 0.1),
    color: theme.palette.secondary.contrastText,
    borderRadius: 12,
    margin: '0.5rem',
    padding: '1rem',
    boxShadow: '0px 8px 12px rgba(0,0,0,0.15)',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '40%',
      margin: 0
    }
  },
  [`& .${classes.button}`]: {
    backgroundColor: theme.palette.background.paper
  },
  [`& .${classes.buttonIcon}`]: {
    color: theme.palette.secondary.main,
    fontSize: '3rem !important',
    [theme.breakpoints.down('sm')]: {
      color: theme.palette.secondary.main,
      fontSize: '2rem !important'
    }
  },
  [`& .${classes.icon}`]: {
    color: theme.palette.secondary.main,
    fontSize: '3rem !important',
    [theme.breakpoints.down('sm')]: {
      color: theme.palette.secondary.main,
      fontSize: '2rem !important'
    }
  }
}))
