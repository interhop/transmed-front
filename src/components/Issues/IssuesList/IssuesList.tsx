import React, { useEffect } from 'react'
import _ from 'lodash'
import { useDispatch } from 'react-redux'

import { Grid, List, ListSubheader, Paper, Skeleton, Typography } from '@mui/material'

import { classes, Root } from './styles'
import Issue from '../Issue/Issue'
import { reTest } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { fetchIssues } from 'state/issues'
import { IssueFilters } from 'services/issues'
import { Issue as IssueType, IssueLabel } from 'types'

type IssuesListProps = {
  issuesIds: string[]
  title: string
  type?: IssueLabel
}

const IssuesList: React.FC<IssuesListProps> = ({ issuesIds, title, type }) => {
  const { issues } = useAppSelector((state) => ({
    issues: state.issues
  }))

  const headerClassNames: Partial<{ [Key in IssueLabel]: string }> = {
    'A-To do': classes.todoHeader,
    'B-Doing': classes.doingHeader,
    'E-Finished': classes.doneHeader,
    'X-Idea': classes.backlogHeader
  }

  const rootClassNames: Partial<{ [Key in IssueLabel]: string }> = {
    'A-To do': classes.todoRoot,
    'B-Doing': classes.doingRoot,
    'E-Finished': classes.doneRoot,
    'X-Idea': classes.backlogRoot
  }

  if (issues.status === 'loading')
    return <Skeleton variant="rounded" width="100%" height={90} sx={{ marginTop: 4, marginBottom: 4 }} />
  if (!issuesIds.length) return <></>

  const displayedIssues = issuesIds
    .filter((id) => id in issues.entities)
    .map((id) => issues.entities[id]) as IssueType[]

  return (
    <Root>
      <Paper className={`${classes.root} ${type ? rootClassNames[type] : rootClassNames['X-Idea']}`}>
        <List
          className={classes.card}
          subheader={
            <ListSubheader
              component="div"
              className={`${classes.unitHeader} ${type ? headerClassNames[type] : headerClassNames['X-Idea']}`}
            >
              <Grid container justifyContent="flex-start" spacing={2} className={classes.subHeaderContent}>
                <Grid item>
                  <Typography align="center" variant="h6" className={classes.listTitle}>
                    {title}
                  </Typography>
                </Grid>
              </Grid>
            </ListSubheader>
          }
        >
          {displayedIssues.map((issue: IssueType) => (
            <Issue key={issue.id} issue={issue} />
          ))}
        </List>
      </Paper>
    </Root>
  )
}

const issueToFilters = (iss: Partial<IssueType>): IssueFilters => {
  if (!iss.title) return {}
  const l = _.uniq(
    iss.title
      .split(' ')
      .map((w) => w.toLowerCase())
      .filter(reTest(/^\w{3,}$/))
  )
  l.sort()
  return {
    search: l.join(','),
    labels: iss.labels?.join(','),
    issueType: iss.issueType,
    in: 'title',
    state: 'opened'
  }
}

const filtersToKey = (f: IssueFilters): string => {
  const res = []
  if (f.issueType) res.push(f.issueType)
  if (f.labels) res.push(f.labels)
  if (f.search) res.push(f.search)
  return res.join(',')
}

type FilteredIssuesListProps = {
  title: string
  type?: IssueLabel
  baseIssue: Partial<IssueType>
}

export const FilteredIssuesList: React.FC<FilteredIssuesListProps & { search?: string }> = ({
  baseIssue,
  ...props
}) => {
  const { issues } = useAppSelector((state) => ({
    issues: state.issues
  }))
  const dispatch = useDispatch<AppDispatch>()

  const [suggestedIssues, setSuggestedIssues] = React.useState<IssueType[]>([])
  const [prevSearched, setPrevSearched] = React.useState<string[]>([])

  useEffect(() => {
    if (!baseIssue.title || baseIssue.title.length <= 5) return
    const search = issueToFilters(baseIssue)
    if (!search.search) return
    const searchKey = filtersToKey(search)
    if (!_.includes(prevSearched, searchKey)) {
      setPrevSearched([...prevSearched, searchKey])
      dispatch(fetchIssues(search))
    }
  }, [baseIssue.title, baseIssue.issueType, (baseIssue.labels ?? [])[0]])

  useEffect(() => {
    const searchedLabels = baseIssue.labels ?? []
    setSuggestedIssues(
      (_.filter(_.values(issues.entities)) as IssueType[]).filter(
        ({ title, labels, issueType }) =>
          _.intersection(baseIssue.title?.toLowerCase().split(' '), title.toLowerCase().split(' ')).length &&
          baseIssue.issueType === issueType &&
          (!searchedLabels.length || searchedLabels.some((label) => _.includes(labels, label)))
      )
    )
  }, [issues.status, baseIssue.title, baseIssue.issueType, (baseIssue.labels ?? [])[0]])

  if (issues.status === 'loading') return <Skeleton variant="rounded" width="80%" height={90} />

  return (
    <Root>
      <IssuesList issuesIds={suggestedIssues.map(({ id }) => id.toString())} {...props} />
    </Root>
  )
}

export default IssuesList
