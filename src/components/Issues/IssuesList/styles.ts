import { styled } from '@mui/material/styles'

const PREFIX = 'IssuesList'
export const classes = {
  root: `${PREFIX}-root`,
  todoRoot: `${PREFIX}-todoRoot`,
  doingRoot: `${PREFIX}-doingRoot`,
  doneRoot: `${PREFIX}-doneRoot`,
  backlogRoot: `${PREFIX}-backlogRoot`,
  card: `${PREFIX}-card`,
  unitHeader: `${PREFIX}-unitHeader`,
  todoHeader: `${PREFIX}-todoHeader`,
  doingHeader: `${PREFIX}-doingHeader`,
  doneHeader: `${PREFIX}-doneHeader`,
  backlogHeader: `${PREFIX}-backlogHeader`,
  listTitle: `${PREFIX}-listTitle`,
  subHeaderContent: `${PREFIX}-subHeaderContent`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.root}`]: {
    marginBottom: '5px',
    marginTop: '5px',
    minWidth: `calc(min(${1000}px, 100%))`,
    borderRadius: 24,
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)',
    [theme.breakpoints.down('sm')]: {
      borderRadius: 12,
      marginBottom: '5px',
      marginTop: '5px',
      width: '100%',
      minWidth: 0
    }
  },
  [`& .${classes.todoRoot}`]: {
    // border: `4px solid ${theme.palette.primary.light}`
    borderLeft: `4px solid ${theme.palette.secondary.main}`,
    borderRight: `4px solid ${theme.palette.secondary.light}`,
    borderBottom: `4px solid ${theme.palette.secondary.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.doingRoot}`]: {
    // border: `4px solid ${theme.palette.primary.light}`
    borderLeft: `4px solid ${theme.palette.secondary.main}`,
    borderRight: `4px solid ${theme.palette.secondary.light}`,
    borderBottom: `4px solid ${theme.palette.secondary.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.doneRoot}`]: {
    // border: `4px solid ${theme.palette.primary.main}`
    borderLeft: `4px solid ${theme.palette.primary.main}`,
    borderRight: `4px solid ${theme.palette.primary.light}`,
    borderBottom: `4px solid ${theme.palette.primary.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.backlogRoot}`]: {
    // border: `4px solid ${theme.palette.secondary.light}`
    borderLeft: `4px solid ${theme.palette.warning.main}`,
    borderRight: `4px solid ${theme.palette.warning.light}`,
    borderBottom: `4px solid ${theme.palette.warning.light}`,
    [theme.breakpoints.down('sm')]: {
      borderLeft: null,
      borderRight: null
    }
  },
  [`& .${classes.card}`]: {
    padding: 0
  },
  [`& .${classes.unitHeader}`]: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    [theme.breakpoints.down('sm')]: {
      borderTopLeftRadius: 8,
      borderTopRightRadius: 8
    }
  },
  [`& .${classes.todoHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.secondary.main} 0%, ${theme.palette.secondary.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.secondary.light, 1)} !important`
  },
  [`& .${classes.doingHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.secondary.main} 0%, ${theme.palette.secondary.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.secondary.light, 1)} !important`
  },
  [`& .${classes.doneHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.primary.main} 0%, ${theme.palette.primary.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.primary.main, 1)} !important`
  },
  [`& .${classes.backlogHeader}`]: {
    backgroundImage: `linear-gradient(90deg, ${theme.palette.warning.main} 0%, ${theme.palette.warning.light} 100%)`
    // backgroundColor: `${alpha(theme.palette.secondary.light, 1)} !important`
  },
  [`& .${classes.listTitle}`]: {
    // backgroundColor: alpha(theme.palette.secondary.main, 0.2),
    color: theme.palette.primary.contrastText,
    borderRadius: 12,
    padding: 5,
    [theme.breakpoints.down('sm')]: {
      borderRadius: 6
    }
  },
  [`& .${classes.subHeaderContent}`]: {
    width: '100%',
    maxWidth: '500px',
    margin: 0,
    padding: 5
  }
}))
