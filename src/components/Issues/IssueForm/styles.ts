import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'IssueForm'
export const classes = {
  card: `${PREFIX}-card`,
  title: `${PREFIX}-title`,
  section: `${PREFIX}-section`,
  emailLink: `${PREFIX}-emailLink`,
  errorText: `${PREFIX}-errorText`,
  validateButton: `${PREFIX}-validateButton`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  margin: theme.spacing(2, 0, 2, 0),
  [`& .${classes.card}`]: {
    padding: 2,
    marginTop: 4,
    marginBottom: 4,
    backgroundColor: theme.palette.background.paper,
    border: `2px solid ${theme.palette.secondary.light}`,
    borderRadius: 12,
    boxShadow: '0px 8px 6px rgba(0,0,0,0.15)'
  },
  [`& .${classes.title}`]: {
    margin: 4,
    width: '100%',
    textAlign: 'center'
  },
  [`& .${classes.section}`]: {
    marginTop: 10,
    marginBottom: 10,
    paddingTop: 4,
    paddingBottom: 4,
    width: '100%'
  },
  [`& .${classes.emailLink}`]: {
    cursor: 'pointer',
    color: theme.palette.secondary.main
  },
  [`& .${classes.errorText}`]: {
    backgroundColor: alpha(theme.palette.error.main, 1),
    color: theme.palette.error.contrastText,
    bold: true
  },
  [`& .${classes.validateButton}`]: {
    width: theme.spacing(60),
    margin: theme.spacing(4),
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    borderRadius: 24,
    '&:hover': {
      backgroundColor: theme.palette.secondary.light
    },
    '&.MuiLoadingButton-loading': {
      color: 'transparent'
    },
    '&.Mui-disabled': {
      backgroundColor: theme.palette.action.disabledBackground
    }
  }
}))
