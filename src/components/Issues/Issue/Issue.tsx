import React from 'react'
import _ from 'lodash'
import { useDispatch } from 'react-redux'

import { ThumbUp as UpVoteIcon } from '@mui/icons-material'
import Badge from '@mui/material/Badge'
import Chip from '@mui/material/Chip'
import CircularProgress from '@mui/material/CircularProgress'
import IconButton from '@mui/material/IconButton'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction'
import Tooltip from '@mui/material/Tooltip'
import Typography from '@mui/material/Typography'
import { Theme, useTheme, useMediaQuery } from '@mui/material'

import { classes, Root } from './styles'
import { AppDispatch, useAppSelector } from 'state'
import { unvoteIssue, voteIssue } from 'state/issues'
import { Issue as IssueType, State } from 'types'
import Markdown from '../../Basic/Markdown/Markdown'

type IssueProps = {
  issue: State<IssueType>
}

const Issue: React.FC<IssueProps> = ({ issue, ...props }) => {
  const th: Theme = useTheme()
  const smallScreen = useMediaQuery(th.breakpoints.down('sm'))
  const { me } = useAppSelector((state) => ({
    me: state.me.user
  }))
  const dispatch = useDispatch<AppDispatch>()

  if (!me) return <></>

  const userHasVoted = _.includes(issue.votes, me.uuid)

  return (
    <Root>
      <ListItemButton key={issue.id} className={classes.issueItem} {...props}>
        {smallScreen ? (
          <>
            <ListItemText
              primary={
                <Tooltip title={<Markdown content={issue.description} />}>
                  <Typography variant="body2">
                    {issue.title} {issue.sp && `(SP: ${issue.sp})`}
                  </Typography>
                </Tooltip>
              }
              secondary={
                <>
                  <a target="_blank" href={issue.webUrl} rel="noreferrer">
                    Voir le ticket
                  </a>
                  {issue.labels.slice(0, 2).map((l) => (
                    <Chip key={l} label={l} className={classes.labelChip} />
                  ))}
                  {issue.labels.length > 2 && <span>...</span>}
                </>
              }
              className={classes.issueDetails}
            />
          </>
        ) : (
          <>
            <ListItemText
              primary={
                <Tooltip title={<Markdown content={issue.description} />}>
                  <Typography variant="body1">{issue.title}</Typography>
                </Tooltip>
              }
              secondary={
                <a target="_blank" href={issue.webUrl} rel="noreferrer">
                  Voir le ticket
                </a>
              }
              className={classes.issueDetails}
            />
            <ListItemText
              primary={
                <>
                  {issue.labels.map((l) => (
                    <Chip key={l} label={l} />
                  ))}
                </>
              }
              secondary={issue.sp ? `Difficulté estimée : ${issue.sp}` : ''}
              className={classes.spDetail}
            />
          </>
        )}
        <ListItemSecondaryAction>
          <IconButton
            title={userHasVoted ? 'Retirer mon vote' : 'Voter'}
            onClick={() => !issue.loading && dispatch(userHasVoted ? unvoteIssue(issue) : voteIssue(issue))}
            disabled={issue.loading}
          >
            {issue.loading ? (
              <CircularProgress className={classes.icon} />
            ) : (
              <Badge badgeContent={issue.votes.length} color="primary">
                <UpVoteIcon className={`${classes.icon} ${userHasVoted ? classes.activatedIcon : ''}`} />
              </Badge>
            )}
          </IconButton>
        </ListItemSecondaryAction>
      </ListItemButton>
    </Root>
  )
}

export default Issue
