import { styled } from '@mui/material/styles'

const PREFIX = 'IssueItem'
export const classes = {
  issueItem: `${PREFIX}-issueItem`,
  issueDetails: `${PREFIX}-issueDetails`,
  spDetail: `${PREFIX}-spDetail`,
  labelChip: `${PREFIX}-labelChip`,
  icon: `${PREFIX}-icon`,
  activatedIcon: `${PREFIX}-activatedIcon`
}

export const Root = styled('div')(({ theme }) => ({
  maxWidth: '100%',
  [`& .${classes.issueItem}`]: {
    height: '60px',
    paddingRight: '66px !important',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  [`& .${classes.issueDetails}`]: {
    width: '20%',
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  [`& .${classes.spDetail}`]: {
    width: '20%',
    [theme.breakpoints.down('sm')]: {
      width: '15%'
    }
  },
  [`& .${classes.labelChip}`]: {
    [theme.breakpoints.down('sm')]: {
      paddingRight: 4,
      paddingLeft: 4,
      height: '1rem',
      fontSize: '0.6rem'
    }
  },
  [`& .${classes.icon}`]: {
    width: '30px',
    height: '100%',
    margin: '2px'
  },
  [`& .${classes.activatedIcon}`]: {
    color: theme.palette.secondary.main
  }
}))
