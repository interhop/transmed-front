import React from 'react'
import { Navigate, useLocation } from 'react-router-dom'

import { useAppSelector } from 'state'
import RoutesConfig from 'components/Routes/AppNavigation/config'

const PrivateRoute: React.FC<any> = ({ children }) => {
  const me = useAppSelector((state) => state.me.user)
  const location = useLocation()
  // const [allowRedirect, setRedirection] = useState(false)

  return me ? (
    children
  ) : (
    // ) : allowRedirect ? (
    <Navigate to={RoutesConfig.login.getPath(location.pathname + location.search)} />
    // ) : (
    //   <Dialog open aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
    //     <DialogTitle id="alert-dialog-title">{''}</DialogTitle>
    //     <DialogContent>
    //       <DialogContentText id="alert-dialog-description">
    //         Il semblerait que vous n'êtes plus connecté. Vous allez être redirigé vers la page de connexion
    //       </DialogContentText>
    //     </DialogContent>
    //     <DialogActions>
    //       <Button onClick={() => setRedirection(true)} color="primary">
    //         Ok
    //       </Button>
    //     </DialogActions>
    //   </Dialog>
  )
}

export default PrivateRoute
