import React, { useState, useEffect, useRef } from 'react'
import { AxiosError, AxiosResponse, isAxiosError } from 'axios'
import jwt, { JwtPayload } from 'jwt-decode'
import { useLocation, useNavigate } from 'react-router-dom'

import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'

import { Root } from './styles'
import RoutesConfig from '../AppNavigation/config'
import env from 'env'
import MyDialogActions from 'components/Dialogs/MyDialogActions/MyDialogActions'
import { useAppSelector, useAppDispatch } from 'state'
import { logout as logoutAction } from 'state/me'
import { getErrorExplicitContent, MessageInput, setMessage } from 'state/message'
import { BackValidationError, RefreshResult } from 'types'
import services from '../../../services'

const checkIntervalSeconds = 10

/**
 * This component listens to the user's clicks and keypresses, and verifies LocalStorage every 20 seconds
 * If :
 * - access token is about to expire (in about 1 minute)
 * - no click or keypress has been made since at least 'env.REACT_APP_INACTIVITY_DELAY_BEFORE_EXPIRATION' seconds
 * then will display a dialog to suggest :
 * - remaining authenticated (via token refresh)
 * - logging out
 * If access token get expired, log out
 */
const AutoLogoutContainer: React.FC<any> = () => {
  const [dialogIsOpen, setDialogIsOpen] = useState(false)
  const [refreshPending, setRefreshPending] = useState(false)

  const location = useLocation()
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const lastActionRef = useRef(0)

  const { me } = useAppSelector((state) => ({ me: state.me.user }))

  const updateLastActionListener = () => {
    if (dialogIsOpen) return
    lastActionRef.current = new Date().valueOf()
  }

  const logout = () => {
    setDialogIsOpen(false)
    dispatch(logoutAction())
    navigate(RoutesConfig.login.getPath(location.pathname + location.search))
  }

  const refreshErrorHandler = (msgInput: MessageInput, additional?: string) => {
    console.error('Could not refresh tokens', msgInput.content, additional)
    dispatch(setMessage(msgInput))
    logout()
  }

  /**
   * Will post a request to refresh tokens, and save result to localStorage
   */
  const stayActive = () => {
    if (refreshPending || localStorage.getItem('refreshPending')) return

    localStorage.setItem('refreshPending', '1')
    setRefreshPending(true)

    services.user
      .refresh()
      .then((resp: AxiosResponse<RefreshResult> | AxiosError<BackValidationError>) => {
        if (isAxiosError(resp)) return refreshErrorHandler(getErrorExplicitContent(resp.response))
        localStorage.setItem('accessToken', resp.data.access)
        setDialogIsOpen(false)
      })
      .catch((e: AxiosError) => {
        refreshErrorHandler(
          { type: 'error', content: 'Erreur pendant la reconnection. Veuillez réessayer.' },
          e.message
        )
      })
      .finally(() => {
        localStorage.removeItem('refreshPending')
        setRefreshPending(false)
      })
  }

  /**
   * Will find accessToken in LocalStorage and decode it
   * If it expired, will log out (by security)
   * If it expires in less than 1mn + 20 seconds (from interval), will :
   * - automatically refresh tokens if the user was active less than 5 minutes ago
   * - else display "stayActive" dialog or
   */
  const checkAccessToken = () => {
    const accessToken = localStorage.getItem('accessToken')
    let jwtAt: any | JwtPayload
    try {
      jwtAt = accessToken && jwt<JwtPayload>(accessToken)
    } catch (e) {
      return
    }
    const now = new Date().valueOf()
    if (!jwtAt || !jwtAt.exp || now > jwtAt.exp * 1000) return logout()

    if (now > (jwtAt.exp - 60 - checkIntervalSeconds) * 1000) {
      // access token will expire in about 1 minute
      if (now < lastActionRef.current + env.REACT_APP_INACTIVITY_DELAY_BEFORE_EXPIRATION * 1000) {
        // user was active less than a certain delay ago
        stayActive()
      } else {
        // we warn client that inactivity is about to force logout
        setDialogIsOpen(true)
      }
    } else {
      setDialogIsOpen(false)
    }
  }

  const cleanEffect = (timer?: NodeJS.Timer) => () => {
    timer && clearInterval(timer)
    window.removeEventListener('keypress', updateLastActionListener)
    window.removeEventListener('click', updateLastActionListener)
  }

  useEffect(() => {
    /**
     * on mounting, will add
     * - an interval that will check access token expiration every ${checkIntervalSeconds} seconds
     * - keyboard and click listeners to remain updated on user activity
     */
    window.addEventListener('keypress', updateLastActionListener)
    window.addEventListener('click', updateLastActionListener)

    const interval = setInterval(() => {
      checkAccessToken()
    }, checkIntervalSeconds * 1000)

    return cleanEffect(interval)
  }, [])

  if (!me) return <></>

  return (
    <Root>
      <Dialog open={dialogIsOpen}>
        <DialogContent>Vous semblez inactif, vous serez déconnecté dans moins d'une minute</DialogContent>
        <DialogActions>
          <MyDialogActions
            onCancel={logout}
            onSubmit={stayActive}
            submitActionTitle="Rester connecté"
            cancelActionTitle="Se déconnecter"
            submitLoading={refreshPending}
          />
        </DialogActions>
      </Dialog>
    </Root>
  )
}

export default AutoLogoutContainer
