import React, { MouseEventHandler } from 'react'

import { Button, Unstable_Grid2 as Grid, SvgIconProps } from '@mui/material'
import { History as HistoryIcon, Save as SaveIcon } from '@mui/icons-material'
import { LoadingButton } from '@mui/lab'

import { classes, Root } from './styles'

type MyDialogActionsProps = {
  onCancel: MouseEventHandler
  onSubmit: MouseEventHandler
  cancelActionTitle?: string
  submitActionTitle?: string
  CancelIcon?: React.FC<SvgIconProps>
  SubmitIcon?: React.FC<SvgIconProps>
  submitLoading?: boolean
  disableSubmit?: boolean
}

const MyDialogActions: React.FC<MyDialogActionsProps> = ({
  CancelIcon = HistoryIcon,
  SubmitIcon = SaveIcon,
  cancelActionTitle = 'Annuler',
  submitActionTitle = 'Enregistrer',
  ...props
}) => {
  return (
    <Root>
      <Grid container justifyContent="space-around" spacing={2}>
        <Grid>
          <Button
            color="secondary"
            variant="outlined"
            startIcon={<CancelIcon />}
            onClick={props.onCancel}
            className={classes.cancelButton}
          >
            {cancelActionTitle}
          </Button>
        </Grid>
        <Grid>
          <LoadingButton
            loading={props.submitLoading}
            color="secondary"
            variant="contained"
            startIcon={<SubmitIcon />}
            onClick={props.onSubmit}
            className={classes.submitButton}
            disabled={props.disableSubmit}
          >
            {submitActionTitle}
          </LoadingButton>
        </Grid>
      </Grid>
    </Root>
  )
}

export default MyDialogActions
