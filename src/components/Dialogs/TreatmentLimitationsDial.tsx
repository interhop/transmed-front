import React from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { Root } from './styles'
import MyDialogActions from './MyDialogActions/MyDialogActions'
import { FormDialog } from 'components/Form'
import {
  TreatmentLimitationFormProps,
  treatmentLimitationFormSchema,
  treatmentLimitationsFormDataToObject,
  treatmentLimitationsObjectToFormData,
  uiTreatmentLimitationFormSchema
} from 'components/formSchemas'
import { AppDispatch, useAppSelector } from 'state'
import { closeTLDial } from 'state/modals'
import { editStayTreatmentLimitation } from 'state/stays'

const TreatmentLimitationsDialog: React.FC<any> = () => {
  const { modal } = useAppSelector((state) => ({
    modal: state.modals.treatmentLimitationDial
  }))
  const dispatch = useDispatch<AppDispatch>()

  const [formData, setFormData] = React.useState<Partial<TreatmentLimitationFormProps>>({})

  const closeDial = () => {
    dispatch(closeTLDial())
  }

  const cancelDial = () => {
    modal.treatmentLimitation && setFormData({})
  }

  const submit = () =>
    !modal.treatmentLimitation
      ? undefined
      : dispatch(
          editStayTreatmentLimitation({
            newLat: { stay: modal.treatmentLimitation.stay, ...treatmentLimitationsFormDataToObject(formData) }
          })
        )

  if (!modal.treatmentLimitation) return <></>

  return (
    <Root>
      <FormDialog
        formProps={{
          schema: treatmentLimitationFormSchema,
          uiSchema: uiTreatmentLimitationFormSchema,
          formData: _.cloneDeep(
            _.isEmpty(formData) ? treatmentLimitationsObjectToFormData(modal.treatmentLimitation) : formData
          ),
          onChange: (form: any) => setFormData(form.formData),
          liveValidate: false
        }}
        onSubmit={submit}
        actions={
          _.isEmpty(formData) ||
          _.isEqual(formData, treatmentLimitationsObjectToFormData(modal.treatmentLimitation)) ? (
            <></>
          ) : (
            <MyDialogActions
              onCancel={cancelDial}
              cancelActionTitle="Rétablir"
              onSubmit={submit}
              submitLoading={modal.loading}
            />
          )
        }
        onClose={closeDial}
        open={modal.open}
      />
    </Root>
  )
}

export default TreatmentLimitationsDialog
