import React from 'react'
import { useDispatch } from 'react-redux'

import { Dialog, DialogContent, Grid } from '@mui/material'

import { Root } from './styles'
import Faq from 'components/Faq/Faq'
import IssueForm from 'components/Issues/IssueForm/IssueForm'
import { AppDispatch, useAppSelector } from 'state'
import { closeHelpDial } from 'state/modals'

const HelpDialog: React.FC<any> = () => {
  const { modal } = useAppSelector((state) => ({
    modal: state.modals.helpDial
  }))
  const dispatch = useDispatch<AppDispatch>()

  return (
    <Root>
      <Dialog title="Help" maxWidth="md" open={modal.open} onClose={() => dispatch(closeHelpDial())}>
        <DialogContent>
          <Grid container justifyContent="center" sx={{ padding: 0 }}>
            <Faq loading={modal.loading} />

            <IssueForm loading={modal.loading} withIssuesLink />
          </Grid>
        </DialogContent>
      </Dialog>
    </Root>
  )
}

export default HelpDialog
