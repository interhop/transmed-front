import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { Root } from './styles'
import MyDialogActions from './MyDialogActions/MyDialogActions'
import { FormDialog } from 'components/Form'
import {
  schemaFailureUpdateSchema,
  schemaFailureUpdateFormDefault,
  uiSchemaFailureUpdateForm,
  FailureUpdateFormProps
} from 'components/formSchemas'
import { displayName } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { closeFailureDial } from 'state/modals'
import { createFailure, editFailure } from 'state/stays'
import { FailureType } from 'types'

const failureNames: { [Key in FailureType]: string } = {
  heart: 'cardiaque',
  metabolic: 'métabolique',
  brain: 'cérébrale',
  lung: 'Défailllance pulmonaire',
  kidney: 'rénale',
  liver: 'hépatique',
  hematologic: 'hématologique',
  detection: ''
}

const FailureUpdateDial: React.FC<any> = () => {
  const { modal, stays } = useAppSelector((state) => ({
    modal: state.modals.updateFailureDial,
    stays: state.stays
  }))
  const dispatch = useDispatch<AppDispatch>()
  const [formData, setFormData] = React.useState<Partial<FailureUpdateFormProps>>({
    ...schemaFailureUpdateFormDefault,
    ...modal.failure
  })

  useEffect(() => {
    setFormData({ ...schemaFailureUpdateFormDefault, ...modal.failure })
  }, [modal])

  const closeDial = () => dispatch(closeFailureDial())

  const cancelDial = () => {
    closeDial()
    setFormData(schemaFailureUpdateFormDefault)
  }

  const submit = () =>
    modal.failure &&
    (modal.failure.uuid
      ? dispatch(
          editFailure({
            failureId: modal.failure.uuid,
            failure: _.pick(formData, ['active', 'indication', 'measureDatetime'])
          })
        )
      : dispatch(
          createFailure({
            failure: { ...modal.failure, ...formData }
          })
        ))

  if (!modal.failure || !stays || !_.includes(_.keys(stays.entities), modal.failure.stay)) return <></>

  return (
    <Root>
      <FormDialog
        formProps={{
          schema: schemaFailureUpdateSchema(
            failureNames[modal.failure.failureType],
            displayName(stays.entities[modal.failure.stay || '']?.patient)
          ),
          uiSchema: uiSchemaFailureUpdateForm,
          formData: _.cloneDeep(formData),
          onChange: (form) => setFormData(form.formData),
          liveValidate: false
        }}
        actions={
          <MyDialogActions
            submitActionTitle="Modifier"
            onSubmit={submit}
            submitLoading={modal.loading}
            onCancel={cancelDial}
          />
        }
        onSubmit={submit}
        open={modal.open}
        onClose={closeDial}
      />
    </Root>
  )
}

export default FailureUpdateDial
