import React, { useEffect, useMemo } from 'react'
import { useDispatch } from 'react-redux'
import { SimpleMdeReact, SimpleMDEReactProps } from 'react-simplemde-editor'
import _ from 'lodash'
import 'easymde/dist/easymde.min.css'
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'

import { Dialog, DialogTitle, DialogContent, DialogActions, Unstable_Grid2 as Grid, Box, useTheme } from '@mui/material'

import { classes, easyMdeStyles, Root } from './styles'
import MyDialogActions from './MyDialogActions/MyDialogActions'
import { dateTimeToString } from 'assets/utils'
import { AppDispatch, useAppSelector } from 'state'
import { closeMarkdownDial } from 'state/modals'
import { editBedStayNote } from 'state/stays'
import { FrontInput, PatientNote } from 'types'

const emptyNote: FrontInput<PatientNote> = { content: '', type: 'todo' }

const MarkdownDial: React.FC<any> = () => {
  const th = useTheme()
  const { modal } = useAppSelector((state) => ({
    modal: state.modals.markdownDial
  }))
  const dispatch = useDispatch<AppDispatch>()
  const [editedData, setEditedData] = React.useState<FrontInput<PatientNote>>(_.cloneDeep(modal.note ?? emptyNote))

  useEffect(() => {
    setEditedData(_.cloneDeep(modal.note ?? emptyNote))
  }, [modal.note?.content])

  const onChangeText = (newText: string) => {
    setEditedData(Object.assign(_.cloneDeep(editedData), { content: newText }))
  }

  const onCancel = () => {
    setEditedData(_.cloneDeep(modal.note ?? _.cloneDeep(emptyNote)))
  }

  const onSubmit = () => {
    dispatch(
      editedData.uuid
        ? editBedStayNote({ newNote: _.pick(editedData, ['content']), oldNoteId: editedData.uuid })
        : editBedStayNote({ newNote: editedData })
    )
  }

  const autofocusCustomOptions = useMemo(() => {
    return {
      autofocus: true,
      spellChecker: false,
      toolbar: [
        'table',
        'bold',
        'italic',
        'strikethrough',
        '|',
        'heading-1',
        'heading-2',
        'heading-3',
        '|',
        'quote',
        'unordered-list',
        'ordered-list',
        'link',
        'horizontal-rule'
      ]
    } as SimpleMDEReactProps['options']
  }, [])

  return (
    <Root>
      <Dialog
        title={modal.note?.type === 'todo' ? 'Todo list' : 'Note de patient'}
        maxWidth="lg"
        open={modal.open}
        onClose={() => dispatch(closeMarkdownDial())}
      >
        <DialogTitle id="form-dialog-title" className={classes.infoText}></DialogTitle>
        <DialogContent className={classes.infoText}>
          Mise à jour: {dateTimeToString(modal.note?.updatedAt)}
          <Grid container>
            <Grid xs={12} sm={6}>
              <Box sx={{ padding: 3 }}>
                <style>{easyMdeStyles(th)}</style>
                <SimpleMdeReact
                  options={autofocusCustomOptions}
                  value={editedData.content}
                  onChange={(text) => onChangeText(text)}
                  className={classes.mde}
                  style={{ backgroundColor: 'transparent !important' }}
                />
              </Box>
            </Grid>
            <Grid xs={12} sm={6}>
              <Box sx={{ padding: 6 }}>
                <ReactMarkdown remarkPlugins={[remarkGfm]}>{editedData.content}</ReactMarkdown>
              </Box>
            </Grid>
          </Grid>
        </DialogContent>
        {editedData.content !== modal.note?.content && (
          <DialogActions>
            <MyDialogActions
              onCancel={onCancel}
              cancelActionTitle="Rétablir"
              onSubmit={onSubmit}
              submitLoading={modal.loading}
            />
          </DialogActions>
        )}
      </Dialog>
    </Root>
  )
}

export default MarkdownDial
