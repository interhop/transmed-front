import React from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import { Root } from './styles'
import MyDialogActions from './MyDialogActions/MyDialogActions'
import { detectionAddFormDefault, detectionAddFormSchema, uiDetectionAddFormSchema } from 'components/formSchemas'
import { FormDialog } from 'components/Form'
import { AppDispatch, useAppSelector } from 'state'
import { closeDetectionAdd } from 'state/modals'
import { createDetection } from 'state/stays'

const DetectionDial: React.FC<any> = () => {
  const { modal } = useAppSelector((state) => ({
    modal: state.modals.detectionAdd
  }))
  const dispatch = useDispatch<AppDispatch>()

  const [formData, setFormData] = React.useState(detectionAddFormDefault())

  const closeDial = () => {
    dispatch(closeDetectionAdd())
  }

  const cancelDial = () => {
    setFormData(detectionAddFormDefault())
    closeDial()
  }

  const submit = () =>
    modal.stay &&
    dispatch(
      createDetection({
        detection: { stay: modal.stay.uuid, ...formData }
      })
    )

  if (!modal.stay) return <></>

  return (
    <Root>
      <FormDialog
        formProps={{
          schema: detectionAddFormSchema,
          uiSchema: uiDetectionAddFormSchema,
          formData: _.cloneDeep(formData),
          onChange: (form) => setFormData(form.formData),
          liveValidate: true
        }}
        onSubmit={submit}
        actions={<MyDialogActions onCancel={cancelDial} onSubmit={submit} submitLoading={modal.loading} />}
        onClose={closeDial}
        open={modal.open}
      />
    </Root>
  )
}

export default DetectionDial
