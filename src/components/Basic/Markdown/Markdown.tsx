import React, { MouseEventHandler } from 'react'
import ReactMarkdown from 'react-markdown'
import remarkGfm from 'remark-gfm'
import { classes, Root } from './styles'

export type MarkdownProps = {
  content: string
  readonly?: boolean
  onDoubleClick?: MouseEventHandler
}

const Markdown: React.FC<MarkdownProps> = ({ content }) => {
  return (
    <Root>
      <ReactMarkdown remarkPlugins={[remarkGfm]} className={classes.text}>
        {content}
      </ReactMarkdown>
    </Root>
  )
}

export default Markdown
