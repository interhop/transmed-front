import React from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'
import { Buffer } from 'buffer'

import { Edit as EditIcon } from '@mui/icons-material'
import Masonry from '@mui/lab/Masonry'
import { Avatar, Box, Chip, Grid, Tooltip } from '@mui/material'

import { classes, Root } from './styles'
import { AppDispatch, useAppSelector } from 'state'
import { openLabelAdd } from 'state/modals'
import { Patient } from 'types'
import CardTitle from 'components/BedStay/CardTitle/CardTitle'

const getShift = (k: string): number => parseInt(new Buffer(k.toLowerCase()).toString('hex'), 16) % 6

type ListLabelsProps = {
  title: string
  field: keyof Patient
  patient: Patient
}

const ListLabels: React.FC<ListLabelsProps> = ({ patient, field, title }) => {
  const { display } = useAppSelector((state) => ({
    display: state.settings
  }))
  const dispatch = useDispatch<AppDispatch>()
  const data: { [Key: string]: string } = _.propertyOf(patient)(field)

  return (
    <Root>
      <Box className={classes.rootBox}>
        <Grid container justifyContent="center" spacing={1}>
          <CardTitle
            title={title}
            canEdit={!display.todoMode}
            onClick={() => dispatch(openLabelAdd({ patient, field }))}
            Icon={EditIcon}
          />
          <Masonry columns={2}>
            {_.entries(data).map(([k, v]) =>
              v ? (
                <Tooltip key={k} title={k}>
                  <Chip
                    size="small"
                    className={classes.coloredChip(getShift(k))}
                    avatar={<Avatar className={classes.avatar}>{k.substr(0, 2)}</Avatar>}
                    label={v}
                  />
                </Tooltip>
              ) : (
                <Chip key={k} size="small" className={classes.coloredChip(getShift(k))} label={k} />
              )
            )}
          </Masonry>
        </Grid>
      </Box>{' '}
    </Root>
  )
}

export default ListLabels
