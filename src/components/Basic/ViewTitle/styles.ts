import { alpha, styled } from '@mui/material/styles'

const PREFIX = 'ViewTitle'
export const classes = {
  card: `${PREFIX}-card`
}

export const Root = styled('div')(({ theme }) => ({
  [`& .${classes.card}`]: {
    backgroundColor: alpha(theme.palette.background.paper, 0.1),
    width: 'auto',
    color: theme.palette.secondary.contrastText,
    borderRadius: 12,
    margin: '0.5rem',
    padding: '1rem',
    boxShadow: '0px 8px 12px rgba(0,0,0,0.15)'
  }
}))
