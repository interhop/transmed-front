import React from 'react'
import { Grid, Typography } from '@mui/material'
import { classes, Root } from './styles'

type ViewTitleProps = {
  title: string
}

const ViewTitle: React.FC<ViewTitleProps> = ({ title }) => {
  return (
    <Root>
      <Grid item container className={classes.card} justifyContent="center" direction="column">
        <Typography align="center" variant="h2">
          {title}
        </Typography>
      </Grid>
    </Root>
  )
}

export default ViewTitle
