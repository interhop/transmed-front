import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { createAccess } from './accesses'
import { createIssue } from './issues'
import {
  closeBedStay,
  createDetection,
  createFailure,
  createPatientAndStay,
  editBedStayNote,
  editFailure,
  editStayTreatmentLimitation,
  moveBedStay,
  updatePatient
} from './stays'
import { Bed, FrontInput, Patient, PatientFailure, PatientNote, Stay, TreatmentLimitation } from 'types'

type ModalsState = {
  bedMove: {
    open: boolean
    stayToMove?: Stay
    loadingMove?: boolean
    loadingRemove?: boolean
  }
  bedUpdate: {
    open: boolean
    bed?: Bed
    loading?: boolean
    fullDisplay: boolean
  }
  detectionAdd: {
    open: boolean
    stay?: Stay
    loading?: boolean
  }
  addLabelDial: {
    open: boolean
    patient?: Patient
    field?: keyof Patient
    loading?: boolean
  }
  markdownDial: {
    open: boolean
    title?: string
    note?: FrontInput<PatientNote>
    loading?: boolean
  }
  treatmentLimitationDial: {
    open: boolean
    treatmentLimitation?: FrontInput<TreatmentLimitation>
    loading?: boolean
  }
  printSettingsDial: {
    open: boolean
    loading?: boolean
  }
  updateFailureDial: {
    open: boolean
    failure?: FrontInput<PatientFailure>
    loading?: boolean
  }
  helpDial: {
    open: boolean
    loading?: boolean
  }
  accessForm: {
    loading: boolean
    accepted?: boolean
  }
}

const modalsSlice = createSlice({
  name: 'modals',
  initialState: {
    bedMove: {
      open: false
    },
    bedUpdate: {
      open: false,
      fullDisplay: true
    },
    detectionAdd: { open: false },
    addLabelDial: { open: false },
    markdownDial: { open: false },
    treatmentLimitationDial: { open: false },
    printSettingsDial: { open: false },
    updateFailureDial: { open: false },
    helpDial: { open: false },
    accessForm: { loading: false }
  } as ModalsState,
  reducers: {
    openBedSwap: (state, action: PayloadAction<Stay>) => ({
      ...state,
      bedMove: { open: true, stayToMove: action.payload }
    }),
    closeBedSwap: (state) => ({ ...state, bedMove: { open: false } }),
    openBedUpdate: (state, action: PayloadAction<{ bed?: Bed; stay?: Stay }>) => ({
      ...state,
      bedUpdate: { ...state.bedUpdate, open: true, bed: action.payload.bed, stay: action.payload.stay }
    }),
    closeBedUpdateDial: (state) => ({ ...state, bedUpdate: { open: false, fullDisplay: state.bedUpdate.fullDisplay } }),
    toggleBedUpdateDisplay: (state) => ({
      ...state,
      bedUpdate: { ...state.bedUpdate, fullDisplay: !state.bedUpdate.fullDisplay }
    }),
    // toggleTodoMode: (state) => ({ ...state, todoMode: !state.todoMode })
    openDetectionAdd: (state, action: PayloadAction<Stay>) => ({
      ...state,
      detectionAdd: { open: true, stay: action.payload }
    }),
    closeDetectionAdd: (state) => ({ ...state, detectionAdd: { open: false } }),
    openFailureDial: (state, action: PayloadAction<FrontInput<PatientFailure>>) => ({
      ...state,
      updateFailureDial: { open: true, failure: action.payload }
    }),
    closeFailureDial: (state) => ({ ...state, updateFailureDial: { ...state.updateFailureDial, open: false } }),
    openLabelAdd: (state, action: PayloadAction<Partial<ModalsState['addLabelDial']>>) => ({
      ...state,
      addLabelDial: { ...action.payload, open: true }
    }),
    closeLabelAdd: (state) => ({ ...state, addLabelDial: { open: false } }),
    openMarkdownDial: (state, action: PayloadAction<Partial<ModalsState['markdownDial']>>) => ({
      ...state,
      markdownDial: { ...action.payload, open: true }
    }),
    closeMarkdownDial: (state) => ({ ...state, markdownDial: { ...state.markdownDial, open: false } }),
    openTLDial: (state, action: PayloadAction<Partial<ModalsState['treatmentLimitationDial']>>) => ({
      ...state,
      treatmentLimitationDial: { ...action.payload, open: true }
    }),
    closeTLDial: (state) => ({ ...state, treatmentLimitationDial: { ...state.treatmentLimitationDial, open: false } }),
    openPSDial: (state) => ({ ...state, printSettingsDial: { open: true } }),
    closePSDial: (state) => ({ ...state, printSettingsDial: { ...state.printSettingsDial, open: false } }),
    openHelpDial: (state) => ({ ...state, helpDial: { open: true } }),
    closeHelpDial: (state) => ({ ...state, helpDial: { ...state.helpDial, open: false } }),
    clearAccessForm: (state) => ({ ...state, accessForm: { loading: false } })
  },
  extraReducers: (builder) => {
    builder
      .addCase(editBedStayNote.pending, (state, action) => {
        if (state.markdownDial.note?.uuid == action.meta.arg.oldNoteId) {
          return {
            ...state,
            markdownDial: { ...state.markdownDial, loading: true }
          }
        }
        return state
      })
      .addCase(editBedStayNote.fulfilled, (state, action) => {
        return {
          ...state,
          markdownDial: { ...state.markdownDial, note: action.payload, loading: false, open: false }
        }
      })
      .addCase(editBedStayNote.rejected, (state, action) => {
        if (state.markdownDial.note?.uuid == action.meta.arg.oldNoteId) {
          return {
            ...state,
            markdownDial: { ...state.markdownDial, loading: false }
          }
        }
        return state
      })
      .addCase(editStayTreatmentLimitation.pending, (state, action) => {
        if (state.treatmentLimitationDial.treatmentLimitation?.uuid == action.meta.arg.oldLatId) {
          return {
            ...state,
            treatmentLimitationDial: { ...state.treatmentLimitationDial, loading: true }
          }
        }
        return state
      })
      .addCase(editStayTreatmentLimitation.fulfilled, (state, action) => {
        return {
          ...state,
          treatmentLimitationDial: {
            ...state.treatmentLimitationDial,
            open: false,
            treatmentLimitation: action.payload,
            loading: false
          }
        }
      })
      .addCase(editStayTreatmentLimitation.rejected, (state, action) => {
        if (state.treatmentLimitationDial.treatmentLimitation?.uuid == action.meta.arg.oldLatId) {
          return {
            ...state,
            treatmentLimitationDial: { ...state.treatmentLimitationDial, loading: false }
          }
        }
        return state
      })
      .addCase(closeBedStay.pending, (state) => ({ ...state, bedMove: { ...state.bedMove, loadingRemove: true } }))
      .addCase(closeBedStay.fulfilled, (state) => ({
        ...state,
        bedMove: { ...state.bedMove, open: false, loadingRemove: false }
      }))
      .addCase(closeBedStay.rejected, (state) => ({
        ...state,
        bedMove: { ...state.bedMove, loadingRemove: false }
      }))
      .addCase(moveBedStay.pending, (state) => ({ ...state, bedMove: { ...state.bedMove, loadingMove: true } }))
      .addCase(moveBedStay.fulfilled, (state) => ({
        ...state,
        bedMove: { ...state.bedMove, open: false, loadingMove: false, stayToMove: undefined }
      }))
      .addCase(moveBedStay.rejected, (state) => ({
        ...state,
        bedMove: { ...state.bedMove, loadingMove: false }
      }))
      .addCase(createPatientAndStay.pending, (state) => ({
        ...state,
        bedUpdate: { ...state.bedUpdate, loading: true }
      }))
      .addCase(createPatientAndStay.fulfilled, (state) => ({
        ...state,
        bedUpdate: { ...state.bedUpdate, open: false, loading: false }
      }))
      .addCase(createPatientAndStay.rejected, (state) => ({
        ...state,
        bedUpdate: { ...state.bedUpdate, loading: false }
      }))
      .addCase(createDetection.pending, (state) => ({
        ...state,
        detectionAdd: { ...state.detectionAdd, loading: true }
      }))
      .addCase(createDetection.fulfilled, (state) => ({
        ...state,
        detectionAdd: { ...state.detectionAdd, open: false, loading: false }
      }))
      .addCase(createDetection.rejected, (state) => ({
        ...state,
        detectionAdd: { ...state.detectionAdd, loading: false }
      }))
      .addCase(updatePatient.pending, (state) => ({
        ...state,
        addLabelDial: { ...state.addLabelDial, loading: true }
      }))
      .addCase(updatePatient.fulfilled, (state) => ({
        ...state,
        addLabelDial: { ...state.addLabelDial, open: false, loading: false }
      }))
      .addCase(updatePatient.rejected, (state) => ({
        ...state,
        addLabelDial: { ...state.addLabelDial, loading: false }
      }))
      .addCase(createFailure.pending, (state) => ({
        ...state,
        updateFailureDial: { ...state.updateFailureDial, loading: true }
      }))
      .addCase(createFailure.fulfilled, (state) => ({
        ...state,
        updateFailureDial: { ...state.updateFailureDial, open: false, loading: false }
      }))
      .addCase(createFailure.rejected, (state) => ({
        ...state,
        updateFailureDial: { ...state.updateFailureDial, loading: false }
      }))
      .addCase(editFailure.pending, (state) => ({
        ...state,
        updateFailureDial: { ...state.updateFailureDial, loading: true }
      }))
      .addCase(editFailure.fulfilled, (state) => ({
        ...state,
        updateFailureDial: { ...state.updateFailureDial, open: false, loading: false }
      }))
      .addCase(editFailure.rejected, (state) => ({
        ...state,
        updateFailureDial: { ...state.updateFailureDial, loading: false }
      }))
      .addCase(createIssue.pending, (state) => ({
        ...state,
        helpDial: { ...state.helpDial, loading: true }
      }))
      .addCase(createIssue.fulfilled, (state) => ({
        ...state,
        helpDial: { ...state.helpDial, open: false, loading: false }
      }))
      .addCase(createIssue.rejected, (state) => ({
        ...state,
        helpDial: { ...state.helpDial, loading: false }
      }))
      .addCase(createAccess.pending, (state) => ({ ...state, accessForm: { loading: true } }))
      .addCase(createAccess.fulfilled, (state) => ({ ...state, accessForm: { accepted: true, loading: false } }))
      .addCase(createAccess.rejected, (state) => ({ ...state, accessForm: { accepted: false, loading: false } }))
  }
})

export default modalsSlice.reducer
export const {
  openBedSwap,
  closeBedSwap,
  openBedUpdate,
  closeBedUpdateDial,
  toggleBedUpdateDisplay,
  openDetectionAdd,
  closeDetectionAdd,
  openLabelAdd,
  closeLabelAdd,
  openMarkdownDial,
  closeMarkdownDial,
  openTLDial,
  closeTLDial,
  openPSDial,
  closePSDial,
  openFailureDial,
  closeFailureDial,
  openHelpDial,
  closeHelpDial,
  clearAccessForm
} = modalsSlice.actions
