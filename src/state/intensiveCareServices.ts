import _ from 'lodash'
import { baseApiModelSelectId, IntensiveCareService } from 'types'
import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'
import { logout } from './me'
import { AppDispatch, RootState } from 'state'

import services from 'services'
import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'
import { fetchUnits } from './units'

export type IntensiveCareServiceState = {
  status: 'idle' | 'loading'
  entities: Dictionary<IntensiveCareService>
  ids: any[]
}

const serviceEntityAdapter: EntityAdapter<IntensiveCareService> = createEntityAdapter({
  selectId: baseApiModelSelectId
})
const defaultInitialState: IntensiveCareServiceState = serviceEntityAdapter.getInitialState({
  status: 'idle'
})

const fetchServices = createAsyncThunk<IntensiveCareService[], undefined, { dispatch: AppDispatch }>(
  'intensiveCareServices/fetchIntensiveCareServices',
  async (arg, thunkAPI) => {
    const resp = await services.intensiveCareServices.fetchIntensiveCareServices().then(({ data }) => data.results)
    resp && thunkAPI.dispatch(fetchUnits({ uuid: _.concat(...resp.map((s) => s.units)).join(',') }))
    return resp
  }
)

const switchService = createAsyncThunk<
  IntensiveCareService,
  { id: string; forceReload?: boolean },
  { state: RootState }
>('intensiveCareServices/fetchIntensiveCareService', async ({ id, forceReload }, { getState }) => {
  const state = getState()

  const shouldRefreshData = !(id in state.intensiveCareServices.entities)

  let service
  if (shouldRefreshData || forceReload) {
    service = await services.intensiveCareServices.fetchIntensiveCareService(id)
  }

  return service ?? state.intensiveCareServices.entities[id]
})

const intensiveCareServicesSlice = createSlice({
  name: 'intensiveCareServices',
  initialState: defaultInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(switchService.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(switchService.fulfilled, serviceEntityAdapter.upsertOne)
      .addCase(fetchServices.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchServices.fulfilled, (state, action) => {
        serviceEntityAdapter.upsertMany(state, action)
        state.status = 'idle'
      })
      .addCase(fetchServices.rejected, (state) => ({ ...state, status: 'idle' }))
  }
})

export default intensiveCareServicesSlice.reducer
export { switchService, fetchServices }
