import { AxiosResponse } from 'axios'
import _ from 'lodash'
import { ActionReducerMapBuilder, AsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

import { AlertColor } from '@mui/lab/Alert/Alert'

import { closeAccess, createAccess, denyAccess, editAccess, removeAccess, validateAccess } from './accesses'
import { fetchBed } from './beds'
import { fetchServices } from './intensiveCareServices'
import { createIssue, unvoteIssue, voteIssue } from './issues'
import { logout, login } from './me'
import {
  closeBedStay,
  createDetection,
  createFailure,
  createPatientAndStay,
  editBedStayNote,
  editFailure,
  editStayTreatmentLimitation,
  fetchStay,
  fetchStays,
  moveBedStay,
  updatePatient,
  updateStay
} from './stays'
import { fetchUsers } from './users'
import { BackValidationError } from '../types'

export type MessageInput = {
  type?: AlertColor
  content: string
}

type MessageState = null | MessageInput

export const getErrorExplicitContent = (payload?: AxiosResponse<BackValidationError>, msg?: string): MessageInput => {
  let content = msg
  if (payload?.status === 500) content = `${content}. Erreur sur le serveur`
  else if (payload?.status === 404)
    content = `${content}. Erreur système l'url suivante n'est pas disponible: ${payload.config.url}`
  else if (payload?.data)
    content = `${content}. Erreur ${payload.status} : ${_.entries(payload?.data)
      .map(([k, v]) => k + ': ' + v)
      .join(' ; ')}`

  content = `${content}. Voir logs pour plus détails.`

  return {
    type: 'error',
    content
  }
}

const addCommonCaseToBuilder = (
  builder: ActionReducerMapBuilder<any>,
  actionCreator: AsyncThunk<any, any, { rejectValue: AxiosResponse<BackValidationError> | undefined }>,
  fulfilledMsg: string,
  rejectedMsg: string
): ActionReducerMapBuilder<any> => {
  builder
    .addCase(actionCreator.fulfilled, () => ({
      type: 'success',
      content: fulfilledMsg
    }))
    .addCase(actionCreator.rejected, (state, action) => {
      console.error('Reducer error in state', rejectedMsg, action)
      return getErrorExplicitContent(action.payload, rejectedMsg)
    })
  return builder
}

const setMessageSlice = createSlice({
  name: 'message',
  initialState: null as MessageState,
  reducers: {
    clearMessage: () => {
      return null
    },
    setMessage: (state: MessageState, action: PayloadAction<MessageState>) => {
      if (!action?.payload) return null
      return {
        type: action.payload.type ?? state?.type ?? 'info',
        content: action.payload.content ?? ''
      }
    }
  },
  extraReducers: (builder) => {
    // todo search about addDefaultCase
    // builder.addDefaultCase()
    builder
      .addCase(login.fulfilled, (state, action) => {
        return {
          type: 'success',
          content: `Connexion réussie. Bonjour ${action.payload.firstName}.`
        }
      })
      .addCase(login.rejected, (state, action) => {
        console.error('login rejected', action)
        let content = 'Une erreur est survenue lors de la connexion'
        if (action.payload?.status === 500) content = 'Erreur sur le serveur'
        else if (action.payload?.data && 'invalid_login' in action.payload.data)
          content = 'Identifiant inconnu ou mot de passe invalide'
        return {
          type: 'error',
          content
        }
      })
      .addCase(logout.fulfilled, () => ({
        type: 'success',
        content: `Déconnecté.e. Au revoir.`
      }))
      .addCase(logout.rejected, () => ({
        type: 'error',
        content: 'Une erreur est survenue lors de la déconnexion'
      }))
      .addCase(fetchServices.rejected, (state, action) => {
        return {
          type: 'error',
          content: `Services non reçus : ${String(action.payload)}`
        }
      })
      .addCase(editBedStayNote.fulfilled, () => ({
        type: 'success',
        content: 'Note modifiée'
      }))
      .addCase(editBedStayNote.rejected, (state, action) => {
        console.error('Error after request editBedStayNote', action)
        return getErrorExplicitContent(action.payload, 'Note non modifiée')
      })
      .addCase(editStayTreatmentLimitation.fulfilled, () => ({
        type: 'success',
        content: 'Limitations de traitement modifiées'
      }))
      .addCase(editStayTreatmentLimitation.rejected, (state, action) => {
        console.error('Error after request editBedStayNote', action)
        return getErrorExplicitContent(action.payload, 'Limitations de traitement non modifiées')
      })
      .addCase(closeBedStay.fulfilled, () => ({
        type: 'success',
        content: 'Séjour clos'
      }))
      .addCase(closeBedStay.rejected, (state, action) => {
        console.error('Error after request closeBedStay', action)
        return getErrorExplicitContent(action.payload, 'Séjour non clos')
      })
      .addCase(moveBedStay.fulfilled, () => ({
        type: 'success',
        content: 'Séjour déplacé'
      }))
      .addCase(moveBedStay.rejected, (state, action) => {
        console.error('Error after request moveBedStay', action)
        return getErrorExplicitContent(action.payload, 'Séjour non déplacé')
      })
      .addCase(fetchBed.rejected, (state, action) => {
        console.error('Error after request fetchBed', action)
        return {
          type: 'error',
          content: `L'obtention du lit a échoué. Voir logs pour plus détails.`
        }
      })
      .addCase(fetchStay.rejected, (state, action) => {
        console.error('Error after request fetchStay', action)
        return {
          type: 'error',
          content: `L'obtention du séjour a échoué. Voir logs pour plus détails.`
        }
      })
      .addCase(fetchStays.rejected, (state, action) => {
        console.error('Error after request fetchStays', action)
        return {
          type: 'error',
          content: `L'obtention des séjours a échoué. Voir logs pour plus détails.`
        }
      })
      .addCase(createPatientAndStay.fulfilled, () => ({
        type: 'success',
        content: 'Patient créé'
      }))
      .addCase(createPatientAndStay.rejected, (state, action) => {
        console.error('Error after request createPatientAndStay', action)
        return getErrorExplicitContent(action.payload, 'Patient non créé')
      })
      .addCase(fetchUsers.rejected, (state, action) => {
        console.error('Error after request fetchUsers', action)
        return getErrorExplicitContent(action.payload, 'Utilisateurs non récupérés')
      })
      .addCase(updatePatient.rejected, (state, action) => {
        console.error('Error after request updatePatient', action)
        return getErrorExplicitContent(action.payload, 'Patient non modifié')
      })
      .addCase(updateStay.rejected, (state, action) => {
        console.error('Error after request updateStay', action)
        return {
          type: 'error',
          content: `Séjour non modifié. Voir logs pour plus détails.`
        }
      })
      .addCase(createFailure.fulfilled, () => ({
        type: 'success',
        content: `Défaillance ajoutée.`
      }))
      .addCase(createFailure.rejected, (state, action) => {
        console.error('Error after request createFailure', action)
        return getErrorExplicitContent(action.payload, 'Défaillance non ajoutée')
      })
      .addCase(editFailure.fulfilled, () => ({
        type: 'success',
        content: `Défaillance modifiée.`
      }))
      .addCase(editFailure.rejected, (state, action) => {
        console.error('Error after request updateFailure', action)
        return getErrorExplicitContent(action.payload, 'Défaillance non modifiée')
      })
      .addCase(createDetection.fulfilled, () => ({
        type: 'success',
        content: `Dépistage ajouté.`
      }))
      .addCase(createDetection.rejected, (state, action) => {
        console.error('Error after request createDetection', action)
        return getErrorExplicitContent(action.payload, 'Dépistage non ajouté')
      })
      .addCase(createAccess.fulfilled, () => ({
        type: 'success',
        content: `Accès demandé.`
      }))
      .addCase(createAccess.rejected, (state, action) => {
        console.error('Error after request createAccess', action)
        return getErrorExplicitContent(action.payload, 'Accès non demandé')
      })
      .addCase(editAccess.fulfilled, () => ({
        type: 'success',
        content: `Accès modifié.`
      }))
      .addCase(editAccess.rejected, (state, action) => {
        console.error('Error after request editAccess', action)
        return getErrorExplicitContent(action.payload, 'Accès non modifié')
      })
      .addCase(denyAccess.fulfilled, () => ({
        type: 'success',
        content: `Accès annulé.`
      }))
      .addCase(validateAccess.fulfilled, () => ({
        type: 'success',
        content: `Accès validé.`
      }))
      .addCase(closeAccess.fulfilled, () => ({
        type: 'success',
        content: `Accès clos.`
      }))
      .addCase(removeAccess.fulfilled, () => ({
        type: 'success',
        content: `Accès supprimé.`
      }))
      .addCase(denyAccess.rejected, (state, action) => {
        console.error('Error after request denyAccess', action)
        return getErrorExplicitContent(action.payload, 'Accès non modifié')
      })
      .addCase(validateAccess.rejected, (state, action) => {
        console.error('Error after request validateAccess', action)
        return getErrorExplicitContent(action.payload, 'Accès non annulé')
      })
      .addCase(closeAccess.rejected, (state, action) => {
        console.error('Error after request closeAccess', action)
        return getErrorExplicitContent(action.payload, 'Accès non validé')
      })
      .addCase(removeAccess.rejected, (state, action) => {
        console.error('Error after request removeAccess', action)
        return getErrorExplicitContent(action.payload, 'Accès non clos')
      })
      .addCase(voteIssue.fulfilled, () => ({
        type: 'success',
        content: `Vote ajouté.`
      }))
      .addCase(voteIssue.rejected, (state, action) => {
        console.error('Error after request voteIssue', action)
        return getErrorExplicitContent(action.payload, 'Vote non ajouté')
      })
      .addCase(unvoteIssue.fulfilled, () => ({
        type: 'success',
        content: `Vote retiré.`
      }))
      .addCase(unvoteIssue.rejected, (state, action) => {
        console.error('Error after request unvoteIssue', action)
        return getErrorExplicitContent(action.payload, 'Vote non retiré')
      })
    addCommonCaseToBuilder(builder, createIssue, 'Ticket ajouté', 'Ticket non ajouté')
    // .addCase(createIssue.fulfilled, () => ({
    //   type: 'success',
    //   content: `Ticket ajouté.`
    // }))
    // .addCase(createIssue.rejected, (state, action) => {
    //   console.error('Error after request createIssue', action)
    //   return getErrorExplicitContent(action.payload, 'Ticket non ajouté')
    // })
  }
})

export default setMessageSlice.reducer
export const { clearMessage, setMessage } = setMessageSlice.actions
