import { AxiosResponse, isAxiosError } from 'axios'
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import { clearLocalStorage } from './storage'
import services from 'services'
import { getCsrfToken } from 'services/api'
import { BackValidationError, User } from 'types'

export type MeState = {
  status: 'idle' | 'loading'
  user: null | User
}

const defaultInitialState: MeState = {
  status: 'idle',
  user: null
}

// Logout action is defined outside the meSlice because it is being used by all reducers
export const logout = createAsyncThunk<null, undefined>('user/logout', async () => {
  clearLocalStorage()
  await services.user.logout()
  return null
})

export interface LoginArgs {
  username: string
  password: string
}

export const login = createAsyncThunk<User, LoginArgs, { rejectValue: AxiosResponse<BackValidationError> | undefined }>(
  'user/login',
  async ({ username, password }, thunkAPI) => {
    getCsrfToken() || (await services.user.getCsrfToken())
    return await services.user
      .login(username, password)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        localStorage.setItem('accessToken', resp.data.accessToken)
        return resp.data.user
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
  }
)

const meSlice = createSlice({
  name: 'me',
  initialState: defaultInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(login.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(login.fulfilled, (state, action) => {
        state.user = action.payload
        state.status = 'idle'
      })
      .addCase(login.rejected, (state) => {
        state.status = 'idle'
      })
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
  }
})

export default meSlice.reducer
