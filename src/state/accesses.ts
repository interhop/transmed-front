import { AxiosResponse, isAxiosError } from 'axios'

import { EntityAdapter } from '@reduxjs/toolkit/src/entities/models'
import { createAsyncThunk, createEntityAdapter, createSlice, Dictionary } from '@reduxjs/toolkit'

import { logout } from './me'
import services from 'services'
import { baseApiModelSelectId, Access, FrontInput, BasicFilter, BackValidationError, State } from 'types'

export type AccessesState = {
  status: 'idle' | 'loading'
  entities: Dictionary<State<Access>>
  ids: any[]
}

const accessEntityAdapter: EntityAdapter<State<Access>> = createEntityAdapter({
  selectId: baseApiModelSelectId
})
const defaultInitialState: AccessesState = accessEntityAdapter.getInitialState({
  status: 'idle'
})

const fetchAccesses = createAsyncThunk<
  Access[],
  BasicFilter<Access>,
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('accesses/fetchAccesses', async (params, thunkAPI) => {
  return await services.accesses
    .fetchAccesses(params)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data.results
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const createAccess = createAsyncThunk<
  Access,
  FrontInput<Access>,
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('accesses/createAccess', async (access, thunkAPI) => {
  return await services.accesses
    .createAccess(access)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const denyAccess = createAsyncThunk<Access, string, { rejectValue: AxiosResponse<BackValidationError> | undefined }>(
  'accesses/denyAccess',
  async (id, thunkAPI) => {
    return await services.accesses
      .denyAccess(id)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
  }
)

const validateAccess = createAsyncThunk<
  Access,
  string,
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('accesses/validateAccess', async (id, thunkAPI) => {
  return await services.accesses
    .validateAccess(id)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const closeAccess = createAsyncThunk<Access, string, { rejectValue: AxiosResponse<BackValidationError> | undefined }>(
  'accesses/closeAccess',
  async (id, thunkAPI) => {
    return await services.accesses
      .closeAccess(id)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return resp.data
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
  }
)

const removeAccess = createAsyncThunk<string, string, { rejectValue: AxiosResponse<BackValidationError> | undefined }>(
  'accesses/removeAccess',
  async (id, thunkAPI) => {
    return await services.accesses
      .deleteAccess(id)
      .then((resp) => {
        if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
        return id
      })
      .catch((e) => thunkAPI.rejectWithValue(e))
  }
)

const editAccess = createAsyncThunk<
  Access,
  { id: string; access: Partial<Access> },
  { rejectValue: AxiosResponse<BackValidationError> | undefined }
>('accesses/editAccess', async ({ id, access }, thunkAPI) => {
  return await services.accesses
    .updateAccess(id, access)
    .then((resp) => {
      if (isAxiosError(resp)) return thunkAPI.rejectWithValue(resp.response)
      return resp.data
    })
    .catch((e) => thunkAPI.rejectWithValue(e))
})

const setEntityLoading = (state: AccessesState, id: string) =>
  accessEntityAdapter.upsertOne(state, {
    ...state.entities[id],
    loading: true
  } as State<Access>)

const setEntityNotLoading = (state: AccessesState, id: string) =>
  accessEntityAdapter.upsertOne(state, {
    ...state.entities[id],
    loading: false
  } as State<Access>)

const accessesSlice = createSlice({
  name: 'accesses',
  initialState: defaultInitialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(logout.fulfilled, () => defaultInitialState)
      .addCase(logout.rejected, () => defaultInitialState)
      .addCase(fetchAccesses.pending, (state) => ({ ...state, status: 'loading' }))
      .addCase(fetchAccesses.fulfilled, (state, action) => {
        accessEntityAdapter.upsertMany(state, action)
        state.status = 'idle'
      })
      .addCase(fetchAccesses.rejected, (state) => ({
        ...state,
        status: 'idle'
      }))
      .addCase(createAccess.fulfilled, accessEntityAdapter.upsertOne)
      .addCase(editAccess.pending, (state, action) => setEntityLoading(state, action.meta.arg.id))
      .addCase(denyAccess.pending, (state, action) => setEntityLoading(state, action.meta.arg))
      .addCase(validateAccess.pending, (state, action) => setEntityLoading(state, action.meta.arg))
      .addCase(closeAccess.pending, (state, action) => setEntityLoading(state, action.meta.arg))
      .addCase(removeAccess.pending, (state, action) => setEntityLoading(state, action.meta.arg))
      .addCase(editAccess.fulfilled, accessEntityAdapter.setOne)
      .addCase(denyAccess.fulfilled, accessEntityAdapter.setOne)
      .addCase(validateAccess.fulfilled, accessEntityAdapter.setOne)
      .addCase(closeAccess.fulfilled, accessEntityAdapter.setOne)
      .addCase(removeAccess.fulfilled, accessEntityAdapter.removeOne)
      .addCase(editAccess.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg.id))
      .addCase(denyAccess.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg))
      .addCase(validateAccess.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg))
      .addCase(closeAccess.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg))
      .addCase(removeAccess.rejected, (state, action) => setEntityNotLoading(state, action.meta.arg))
  }
})

export default accessesSlice.reducer
export { fetchAccesses, createAccess, editAccess, denyAccess, validateAccess, closeAccess, removeAccess }
