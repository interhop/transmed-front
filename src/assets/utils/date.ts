import moment from 'moment'

export const dateTimeToString = (value?: Date | string | number): string => {
  if (!value) return ''
  const date = new Date(value)
  return (
    date.toLocaleDateString('fr-FR') +
    ' à ' +
    date.toLocaleTimeString('fr-FR', {
      hour: '2-digit',
      minute: '2-digit'
    })
  )
}

/**
 * Returns how many days spent between value and now, under the format 'Jxx'
 * @param {Date} value
 */
export const dateToDayStep = (value?: string | Date | number): string => {
  if (!value) return ''
  value = new Date(value)
  const actValue = new Date(value.getFullYear(), value.getMonth(), value.getDate())
  return `J${Math.floor((new Date().valueOf() - new Date(actValue).valueOf()) / 86400000) + 1}`
}

export const dateToStr = (date?: string | Date | number): string => {
  if (!date) return ''
  const options: Intl.DateTimeFormatOptions = {
    weekday: 'short',
    month: 'short',
    day: '2-digit'
  }

  return new Date(date).toLocaleDateString('fr', options)
}

export const dateTimeToStr = (date?: string | Date | number): string => {
  if (!date) return ''
  const options: Intl.DateTimeFormatOptions = {
    weekday: 'short',
    month: 'short',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit'
  }

  return new Date(date).toLocaleDateString('fr', options)
}

export const toISOLocale = (date?: string | Date | number): string => {
  if (!date) return ''
  return moment(date).local().format('YYYY-MM-DDTHH:mm:ss')
}

export const toISOLocaleDate = (date?: string | Date | number): string => {
  if (!date) return ''
  return moment(date).local().format('YYYY-MM-DD')
}

export const isSameDate = (a: string | Date | number, b: string | Date | number): boolean =>
  toISOLocaleDate(a) === toISOLocaleDate(b)

export const dateTimeToFileName = (date: string | Date | number): string => {
  if (!date) return ''
  return new Date(date)
    .toISOString() //YYYY-MM-DDThh:mm:ss.mmmZ
    .replace(/...\..*/g, '')
    .replace(/[-T:]/g, '_')
}

export const getAge = (birthDate?: Date | string | number): number | null => {
  if (!birthDate) return null
  return Math.floor((new Date().valueOf() - new Date(birthDate).valueOf()) / 31557600000)
}

/**
 * Return nb of days between two dates, not considering times (only one day between 2020/01/01T00:00 and 2020/01/02T23:59)
 * @param {Date || string} firstDate
 * @param {Date || string} secondDate
 */
export const nbDaysBetween = (firstDate?: Date | string | number, secondDate?: Date | string | number): number => {
  if (!firstDate || !secondDate) return 0
  firstDate = setToMidnight(firstDate)
  secondDate = setToMidnight(secondDate)

  return Math.round((secondDate.valueOf() - firstDate.valueOf()) / 86400000)
}

export const setToMidnight = (date: Date | string | number): Date => {
  const d = new Date(date)
  return new Date(d.getFullYear(), d.getMonth(), d.getDate())
}
