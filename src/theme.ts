import { alpha, createTheme, PaletteMode, responsiveFontSizes } from '@mui/material'
import { frFR } from '@mui/material/locale'

import SpaceGrotesk from 'assets/fonts/SpaceGrotesk-VariableFont_wght.ttf'

export const theme = (mode: PaletteMode) =>
  responsiveFontSizes(
    createTheme(
      {
        palette: {
          mode,
          ...(mode === 'light'
            ? {
                // based on paletton : 545794
                // 60 30 10 : https://www.flowmapp.com/blog/glossary-term/60-30-10-rule
                primary: {
                  main: '#6C8DA2',
                  contrastText: '#FFFFFF',
                  dark: '#476F89',
                  light: '#9EB7C6'
                },
                secondary: {
                  main: '#8B75AC',
                  contrastText: '#FFFFFF',
                  dark: '#694E91',
                  light: '#B5A5CD'
                },
                error: {
                  dark: '#AB5639',
                  main: '#D5856A',
                  light: '#FDBCA5'
                },
                warning: {
                  dark: '#AB8D39',
                  main: '#D6B969',
                  light: '#FDE6A5'
                },
                background: {
                  default: '#476F89',
                  paper: '#FFFFFF' // '#FFFBF6' //'#FFF1C8'
                },
                text: {
                  disabled: 'rgb(0, 0, 0, 0.54)',
                  primary: '#000000',
                  secondary: '#000000'
                }
              }
            : {
                primary: {
                  main: '#486F88',
                  contrastText: '#FFFFFF',
                  dark: '#29526D',
                  light: '#6D8DA2'
                },
                secondary: {
                  main: '#694E91',
                  contrastText: '#FFFFFF',
                  dark: '#4A2E74',
                  light: '#8B75AC'
                },
                error: {
                  dark: '#AB8D39',
                  main: '#D6B969',
                  light: '#FDE6A5'
                },
                warning: {
                  dark: '#AB5639',
                  main: '#D5856A',
                  light: '#FDBCA5'
                },
                background: {
                  default: '#29526D',
                  paper: '#0C1D28' // '#FFFBF6' //'#FFF1C8'
                },
                text: {
                  disabled: alpha('#9EB7C6', 0.54),
                  primary: '#9EB7C6',
                  secondary: '#9EB7C6'
                }
              })
        },
        typography: {
          h1: {
            fontSize: '3rem'
          },
          h2: {
            fontSize: '2rem'
          },
          h3: {
            fontSize: '1.5rem'
          },
          body1: {
            fontSize: '0.8rem',
            '@media (min-width:600px)': {
              fontSize: '1rem'
            }
          },
          body2: {
            fontSize: '0.7rem',
            '@media (min-width:600px)': {
              fontSize: '0.8rem'
            }
          },
          fontFamily: 'SpaceGrotesk'
        },
        spacing: 2,
        components: {
          MuiCssBaseline: {
            styleOverrides: `
              @font-face {
                font-family: 'SpaceGrotesk';
                font-style: normal;
                font-display: swap;
                font-weight: 400;
                src: local('SpaceGrotesk'), local('SpaceGrotesk-Regular'), url(${SpaceGrotesk}) format('woff2');
                unicodeRange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
              }
            `
          }
          // MuiTypography: {
          //     styleOverrides: {
          //       body2: {
          //         fontSize: '0.7rem'
          //       }
          //     }
          //   }
        }
      },
      frFR
    )
  )

export default theme
