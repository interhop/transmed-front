import React, { useEffect } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

import { CssBaseline, GlobalStyles, ThemeProvider } from '@mui/material'

import './design.scss'
import { theme } from './theme'
import AppNavigation from 'components/Routes/AppNavigation/AppNavigation'
import { progressBetweenColors } from 'assets/utils'
import { useAppSelector } from 'state'
import { store, persistor } from 'state/store'

// const authClient = new ApolloClient({
//   cache: new InMemoryCache(),
//   uri: AUTH_API_URL
// })
// moment.locale('fr')

const WithTheme: React.FC<any> = (props) => {
  const nightMode = useAppSelector((state) => state.settings.nightMode)
  const myTheme = theme(nightMode ? 'dark' : 'light')

  useEffect(() => undefined, [nightMode])

  const makeGradient = (dark: boolean): string => {
    let c1, c2, c3
    if (dark) {
      ;[c1, c2, c3] = [myTheme.palette.primary.dark, myTheme.palette.primary.main, myTheme.palette.secondary.main]
    } else {
      ;[c1, c2, c3] = [myTheme.palette.primary.dark, myTheme.palette.primary.main, myTheme.palette.secondary.main]
    }
    // return `linear-gradient(62deg, #4998c5 0%, #6c8ea2 60%, #8e75ad 100%)`
    // return `linear-gradient(62deg, ${c1} 0%, ${c2} 60%, ${c3} 100%)`
    // return `linear-gradient(180deg, ${c2} 0%, ${c2} 10%, ${c1} 90%, ${c1} 91%, ${c1} 100%)`
    // return `radial-gradient(ellipse at left top, ${c2} 0%, ${c1} 40%`

    return `radial-gradient(ellipse at left top, ${c2} 0%, ${c1} 30%, ${progressBetweenColors(
      c1,
      c3,
      0.2
    )} 60%, ${c1} 70%, ${c1} 100%`
  }

  return (
    <ThemeProvider theme={myTheme}>
      <GlobalStyles
        styles={{
          body: {
            backgroundColor: myTheme.palette.primary.dark
          }
        }}
      />
      <div
        style={{
          height: '100vh',
          width: '100%',
          position: 'sticky',
          backgroundPosition: 'center',
          backgroundRepeat: 'noRepeat',
          backgroundSize: 'cover',
          backgroundColor: myTheme.palette.primary.dark,
          backgroundImage: makeGradient(nightMode)
          // backgroundColor: '#6c8ea2',
          // backgroundImage: 'linear-gradient(62deg, #6c8ea2 32%, #4998c5 59%, #ddd4e9 89%)'
        }}
      >
        {props.children}
      </div>
    </ThemeProvider>
  )
}

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <WithTheme>
          <CssBaseline />
          <AppNavigation />
        </WithTheme>
      </PersistGate>
    </Provider>
  )
}

export default App
