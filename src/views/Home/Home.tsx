import React from 'react'
import { Navigate } from 'react-router-dom'

const Home = () => {
  return (
    <Navigate
      to={{
        pathname: '/int-care-services'
        // state: { from: location }
      }}
    />
  )
}

export default Home
