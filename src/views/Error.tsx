import React from 'react'

export default function () {
  return <p>Accès interdit : contacter un administrateur si vous pensez qu'il y a un problème.</p>
}
