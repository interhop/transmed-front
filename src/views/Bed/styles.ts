import { styled } from '@mui/material/styles'

const PREFIX = 'BedView'
export const classes = {
  cardContainer: `${PREFIX}-cardContainer`,
  card: `${PREFIX}-card`
}

export const Root = styled('div')(({ theme }) => ({
  width: '100%',
  [`& .${classes.cardContainer}`]: {
    [theme.breakpoints.down('sm')]: {
      padding: '0px !important'
    }
  },
  [`& .${classes.card}`]: {
    padding: 2,
    [theme.breakpoints.down('sm')]: {
      border: 0
    }
  }
}))
