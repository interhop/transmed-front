import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import _ from 'lodash'

import Grid from '@mui/material/Unstable_Grid2'

import ViewTitle from 'components/Basic/ViewTitle/ViewTitle'
import IssueForm from 'components/Issues/IssueForm/IssueForm'
import IssuesList from 'components/Issues/IssuesList/IssuesList'
import { AppDispatch, useAppSelector } from 'state'
import { fetchIssues } from 'state/issues'
import { Issue } from 'types'

const Issues = () => {
  const { issues, modal } = useAppSelector((state) => ({
    issues: state.issues,
    modal: state.modals.helpDial
  }))
  const dispatch = useDispatch<AppDispatch>()

  useEffect(() => {
    if (issues.status !== 'loading')
      dispatch(
        fetchIssues({
          state: 'opened',
          labels: 'j-User request'
        })
      )
  }, [])

  const myIssues = (_.filter(_.values(issues.entities)) as Issue[]).filter(({ labels }) =>
    _.includes(labels, 'j-User request')
  )
  const unseenIssues = myIssues
    .filter(({ labels }) => _.includes(labels, 'X-Idea'))
    .filter(_.matches({ state: 'opened' }))
    .map(({ id }) => id.toString())
  const backlogIssues = myIssues
    .filter(({ labels }) => _.intersection(labels, ['Y-Validated', 'Z-Defined']).length)
    .map(({ id }) => id.toString())
  const todoIssues = myIssues.filter(({ labels }) => _.includes(labels, 'A-To do')).map(({ id }) => id.toString())
  const doingIssues = myIssues
    .filter(({ labels }) => ['C-Code review', 'B-Doing'].some((l) => _.includes(labels, l)))
    .map(({ id }) => id.toString())
  const doneIssues = myIssues
    .filter(({ labels }) => ['E-Finished', 'D-Qualification'].some((l) => _.includes(labels, l)))
    .map(({ id }) => id.toString())

  return (
    <>
      <Grid container sm={12} justifyContent="center">
        <ViewTitle title="Gestion des tickets" />
      </Grid>

      <IssuesList issuesIds={[...unseenIssues, ...backlogIssues]} title="En attente" type="X-Idea" />
      <IssuesList issuesIds={[...todoIssues, ...doingIssues]} title="En cours de développement" type="B-Doing" />
      <IssuesList issuesIds={doneIssues} title="Dans la prochaine version" type="E-Finished" />
      <IssueForm loading={modal.loading} />
    </>
  )
}

export default Issues
