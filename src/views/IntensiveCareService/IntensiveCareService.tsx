import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import { Navigate, useNavigate, useParams } from 'react-router-dom'

import { Button, Unstable_Grid2 as Grid, LinearProgress, Typography } from '@mui/material'
import { useAppDispatch, useAppSelector } from 'state'

import MarkdownDial from 'components/Dialogs/MarkdownDial'
import DetectionDial from 'components/Dialogs/DetectionsDial'
import BedDialog from 'components/Dialogs/BedDial'
import BedSwapDial from 'components/Dialogs/BedSwapDial'
import FailureUpdateDial from 'components/Dialogs/FailureUpdateDial'
import Title from 'components/CareService/Title/Title'
import Unit from 'components/CareService/Unit/Unit'
import RoutesConfig from 'components/Routes/AppNavigation/config'

import { isAccessValid } from 'assets/utils'
import { fetchAccesses } from 'state/accesses'
import { fetchServices } from 'state/intensiveCareServices'
import { Access, IntensiveCareService as IntensiveCareServiceType } from 'types'

const IntensiveCareService = () => {
  const { serviceId } = useParams<{
    serviceId?: string
  }>()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const { services, accesses } = useAppSelector((state) => ({
    services: state.intensiveCareServices,
    accesses: state.accesses
  }))

  const [stateService, setStateService] = useState<IntensiveCareServiceType | undefined>(
    services.entities[serviceId || '']
  )

  useEffect(() => {
    if (accesses.status !== 'loading' && !accesses.ids.length) {
      dispatch<any>(fetchAccesses({}))
    }
    if (services.status !== 'loading') {
      if (serviceId) {
        serviceId in services.entities || dispatch<any>(fetchServices())
      } else {
        dispatch<any>(fetchServices())
      }
    }
  }, [])
  useEffect(() => {
    serviceId && setStateService(services.entities[serviceId || ''])
  }, [serviceId, services])

  if (accesses.status === 'loading' || services.status === 'loading') return <LinearProgress />

  const myValidAccesses = _.values(accesses.entities)
    .filter(_.matches({ rightEdit: true }))
    .filter(isAccessValid) as Access[]
  const myServices = (_.filter(_.values(services.entities)) as IntensiveCareServiceType[]).filter(({ uuid }) =>
    _.includes(_.map(myValidAccesses, 'intensiveCareService'), uuid)
  )

  if (_.keys(myServices).length === 0) {
    return (
      <>
        <Typography>Vous n'avez accès à aucun service.</Typography>
        <Button onClick={() => window.open(`/accesses`, '_blank')}>Demander un accès</Button>
      </>
    )
  }

  if (!serviceId && myServices.length)
    return <Navigate to={RoutesConfig.intensiveCareService.getPath(myServices.at(0)?.uuid)} />
  if (!stateService) return <Typography>Service introuvable</Typography>

  return (
    <>
      <Grid container xs={12} justifyContent="center">
        <Title
          service={stateService}
          otherServices={myServices}
          changeService={(sId) => navigate(RoutesConfig.intensiveCareService.getPath(sId))}
        />
      </Grid>
      {(stateService.units ?? []).map((unitId) => (
        <Unit key={unitId} unitId={unitId} />
      ))}
      {/*<Grid xs={12} sm={6} container justifyContent="center" spacing={1}>*/}
      {/*  <DebugDisplay service={services.entities[serviceId ?? '']} />*/}
      {/*</Grid>*/}
      <BedSwapDial />
      <BedDialog />
      <MarkdownDial />
      <FailureUpdateDial />
      <DetectionDial />
    </>
  )
}

export default IntensiveCareService
