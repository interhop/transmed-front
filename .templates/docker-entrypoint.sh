#!/bin/sh
set -e

# Replace all URLs in nginx.conf by environment variables
sed -i "s@{BACK_URL}@$BACK_URL@g" /etc/nginx/conf.d/nginx.conf

# Restart nginx to apply changes
service nginx restart

# Sleep infinity so the container will run forever
sleep infinity